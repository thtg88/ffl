<div class="row">
	<div class="col-sm-12">
		<form method="POST" action="{{ url('offers') }}" class="form-horizontal" role="form">
			{!! csrf_field() !!}
			<input type="hidden" name="market_id" value="{{ $first_next_market->id }}">
			<div class="form-group">
				<!-- Player -->
				<div class="col-sm-4{{ $errors->has('player_id') ? ' has-error' : '' }}">
					<label for="offer-player_id" class="control-label">Name</label>
					<select name="player_id" id="offer-player_id" tabindex="1" value="{{ old('player_id') }}" class="form-control">
						<option value="">Please select a Player...</option>
						@foreach($positions as $position)
							<optgroup label="{{ $position->name }}s">
								@foreach($avail_players as $player)
									@if($player->position->id == $position->id)
										<option value="{{ $player->id }}"{{ old('player_id') == $player->id ? ' selected="selected"' : '' }}>{{ $player->name.' - '.$player->team->name.' - '.$player->cost }}</option>
									@endif
								@endforeach
							</optgroup>
						@endforeach
					</select>
					@if($errors->has('player_id'))
						<span class="help-block">
							<strong>{{ $errors->first('player_id') }}</strong>
						</span>
					@endif
				</div>
				<!-- Amount Offered -->
				<div class="col-sm-4{{ $errors->has('amount_offered') ? ' has-error' : '' }}">
					<label for="offer-amount_offered" class="control-label">Amount</label>
					<input type="text" name="amount_offered" id="offer-amount_offered" value="{{ old('amount_offered') }}" tabindex="2" class="form-control">
					@if($errors->has('amount_offered'))
						<span class="help-block">
							<strong>{{ $errors->first('amount_offered') }}</strong>
						</span>
					@endif
				</div>
				<!-- Add Offer -->
				<div class="col-sm-4{{ $errors->has('fantateam_id') ? ' has-error' : '' }}">
					<input type="hidden" name="fantateam_id" id="offer-fantateam_id" value="{{ $fantateam->id }}">
					<div class="btn-group btn-group-justified">
						<div class="btn-group">
							<button type="submit" tabindex="3" class="btn btn-success">
								<i class="fa fa-plus"></i> Add Offer
							</button>
						</div>
					</div>
					@if($errors->has('fantateam_id'))
						<span class="help-block">
							<strong>{{ $errors->first('fantateam_id') }}</strong>
						</span>
					@endif
				</div>
			</div>
		</form>
	</div>
</div>
