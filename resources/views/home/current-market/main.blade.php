@if(count($positions) == count($position_ids_to_remove))
	@include('home.alerts.all-offers-done')
@endif
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">The Max spend per player is: {{ $max_spend_per_player }} - Your current budget is: {{ $fantateam->budget }} - {{ $first_next_market->name }} Market - {{ $active_season->name }} Season</h4>
			</div>
			<div class="panel-body">
				@include('home.current-market.offers-list')
				@if(count($positions) != count($position_ids_to_remove))
					@include('home.current-market.add-offer')
				@endif
			</div>
		</div>
	</div>
</div>
@section('scripts')
<script src="{{ asset('js/markets/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/markets/onload.js?v='.config('app.version')) }}"></script>
@endsection
