<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
			<table id="markets-table" class="table table-hover market-table">
				<thead>
					<tr>
						<th>Position</th>
						<th>Player</th>
						<th>Amount</th>
						<th>Am I Winning?</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($my_offers as $idx => $offer)
						<tr class="{{ $offer->is_max ? 'success' : 'danger' }}">
							<!-- Offer Player Position -->
							<td class="table-text col-sm-2">
								<div>{{ $offer->player->position->abbreviation }}</div>
							</td>
							<!-- Offer Player Name -->
							<td class="table-text col-sm-4">
								<div>{{ $offer->player->name }}</div>
							</td>
							<!-- Offer Started At -->
							<td class="table-text col-sm-2">
								<div>{{ $offer->amount_offered }}</div>
							</td>
							<!-- Offer Am I Winning? -->
							<td class="table-text col-sm-2">
								<div>{{ $offer->is_max ? 'Yes' : 'No' }}</div>
							</td>
							<td class="col-sm-2">
								<div class="btn-toolbar" role="toolbar">
									<!-- View Offers for Player Button -->
									<div class="btn-group">
										<a href="{{ url('markets/'.$first_next_market->id.'/players/'.$offer->player->id) }}" title="View Offers for this Player" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-usd"></span></a>
									</div>
									<!-- Delete Offer Button -->
									<div class="btn-group">
										<form action="{{ url('offers/'.$offer->id) }}" method="POST">
											{!! csrf_field() !!}
											{!! method_field('DELETE') !!}
											<button title="Remove Offer" class="btn btn-xs btn-danger">
												<span class="glyphicon glyphicon-remove"></span>
											</button>
										</form>
									</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
