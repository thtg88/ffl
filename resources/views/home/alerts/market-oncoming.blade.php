<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-warning text-center" role="alert">
			<p>A Market is about to start on {{ $first_next_market->started_at }}.</p>
			<p>You can register below your offers now!</p>
		</div>
	</div>
</div>
