<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-warning text-center" role="alert">
			<p>The season is over. Wait for the next season to start.</p>
		</div>
	</div>
</div>
