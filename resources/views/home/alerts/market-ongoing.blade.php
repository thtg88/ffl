<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-warning text-center" role="alert">
			<p>There's an ongoing Market, finishing at {{ $first_next_market->finished_at }}.</p>
			<p>Hurry up! The player you want might be really close to sign a deal with your Fantateam rivals!</p>
		</div>
	</div>
</div>
