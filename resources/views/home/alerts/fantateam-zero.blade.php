<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-info text-center" role="alert">
			<p>The {{ $active_season->name }} season is about start!</p>
			<p>Please register your Fantasy Team name below.</p>
		</div>
	</div>
</div>
