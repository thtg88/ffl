<div class="row">
  <div class="col-sm-12">
    <div class="alert alert-info text-center">
      <p>You have submitted all your offers.</p>
      <p>In order to edit any of those please remove it and re-add it with the desired amount or open the player's offers screen by clicking on the $ sign in the table below.</p>
    </div>
  </div>
</div>
