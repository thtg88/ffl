<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-danger text-center" role="alert">
			<p>No Players available in the system for this Season.</p>
			<p>Please wait for the Master to upload them.</p>
		</div>
	</div>
</div>
