<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-warning text-center" role="alert">
			<p>You haven't been registered for a season yet.</p>
		</div>
	</div>
</div>
