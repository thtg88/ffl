@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>Home - The Fantasy Football League</h1>
</div>
@if(empty($active_season) && count($ended_seasons) == 0)
	@include('home.alerts.seasons-zero')
@elseif(empty($active_season))
	@include('home.alerts.season-ended')
@else
	@if(empty($fantateam))
		@include('home.alerts.fantateam-zero')
	@else
		@if(empty($prev_markets) && empty($first_next_market))
			@include('home.alerts.markets-zero')
		@elseif(empty($first_next_market) === FALSE)
			@if($first_next_market->started_at > $now)
				@include('home.alerts.market-oncoming')
			@else
				@include('home.alerts.market-ongoing')
			@endif
			@if(count($all_players) == 0)
				@include('home.alerts.players-zero')
			@else
				@include('home.current-market.main')
			@endif
		@endif
	@endif
	<!-- Move to Settings once a Market is Finished-->
	@include('home.fantateam')
@endif
@endsection
