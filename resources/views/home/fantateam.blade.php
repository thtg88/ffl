<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Fantasy Team Details</h4>
			</div>
			<div class="panel-body">
				<form action="{{ url('fantateams'.(empty($fantateam) ? '' : '/'.$fantateam->id)) }}" method="POST" class="form-horizontal" role="form">
					{!! csrf_field() !!}
					@if(empty($fantateam))
						<input type="hidden" name="season_id" value="{{ $active_season->id }}">
					@else
						{!! method_field('PUT') !!}
					@endif
					<div class="form-group">
						<div class="col-sm-6{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="fantateam-name" class="control-label">Name</label>
							<input type="text" name="name" id="fantateam-name" value="{{ empty($fantateam) ? old('name') : $fantateam->name }}" class="form-control" tabindex="4">
							@if($errors->has('name'))
								<p class="help-block">{{ $errors->first('name') }}</p>
							@endif
						</div>
						<div class="col-sm-3{{ $errors->has('formation_id') ? ' has-error' : '' }}">
							<label for="fantateam-formation" class="control-label">Favourite Formation</label>
							<select name="formation_id" id="fantateam-formation" class="form-control" tabindex="6">
								<option value="">Please select a Favourite Formation...</option>
								@foreach($formations as $formation)
									<option value="{{ $formation->id }}"{{ empty($fantateam) || $fantateam->formation_id != $formation->id ? '' : ' selected="selected"' }}>{{ $formation->name }}</option>
								@endforeach
							</select>
							@if($errors->has('formation_id'))
								<p class="help-block">{{ $errors->first('formation_id') }}</p>
							@endif
						</div>
						<div class="col-sm-3{{ $errors->has('budget') ? ' has-error' : '' }}">
							<label for="fantateam-budget" class="control-label">Budget</label>
							<input type="text" name="budget" id="fantateam-budget" value="{{ empty($fantateam) ? $active_season->fantateam_budget : $fantateam->budget }}" class="form-control" tabindex="7">
							@if($errors->has('budget'))
								<p class="help-block">{{ $errors->first('budget') }}</p>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-9 col-sm-3">
							<input type="hidden" name="user_id" class="form-control" value="{{ $user->id }}">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<button class="btn btn-success" tabindex="7">
										<span class="glyphicon glyphicon-floppy-disk"></span>{{ empty($fantateam) ? ' Register Team' : ' Save Changes' }}
									</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
