@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>403</h1>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-danger text-center">
			@if(empty($exception->getMessage()) === FALSE)
				<p>{{ $exception->getMessage() }}</p>
			@else
				<p>You are unauthorized to perform this action.</p>
			@endif
		</div>
	</div>
</div>
@endsection
