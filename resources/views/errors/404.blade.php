@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>404</h1>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-danger text-center">
			@if(empty($exception->getMessage()) === FALSE)
				<p>{{ $exception->getMessage() }}</p>
			@else
				<p>Page not found.</p>
			@endif
		</div>
	</div>
</div>
@endsection
