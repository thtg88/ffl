<div class="row">
  <div class="col-sm-12">
    <div class="alert alert-success text-center">
      <p>Market closed successfully. How about <a href="{{ url('admin/seasons/'.$season->id) }}">creating the calendar?</p>
    </div>
  </div>
</div>
