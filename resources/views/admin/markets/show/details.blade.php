<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">Market Details</h4>
			</div>
			<div class="panel-body">
				<form action="{{ url('markets/'.$market->id) }}" method="POST" class="form-horizontal">
					{!! csrf_field() !!}
					{!! method_field('PUT') !!}
					<div class="form-group">
						<div class="col-sm-6{{ $errors->has('started_at') ? ' has-error' : '' }}">
							<label for="market-started_at" class="control-label">Starting Date</label>
							@if(!$season->is_active || $market->started_at <= $now && $now < $market->finished_at)
								<input type="date" name="started_at" id="market-started_at" value="{{ empty($market->started_at) ? '' : $market->started_at->toDateString() }}" disabled="disabled" tabindex="1" class="form-control">
							@else
								<input type="date" name="started_at" id="market-started_at" value="{{ empty($market->started_at) ? '' : $market->started_at->toDateString() }}" tabindex="1" class="form-control">
							@endif
							@if($errors->has('started_at'))
								<span class="help-block">
									<strong>{{ $errors->first('started_at') }}</strong>
								</span>
							@endif
						</div>
						<div class="col-sm-6{{ $errors->has('finished_at') ? ' has-error' : '' }}">
							<label for="market-finished_at" class="control-label">Finishing Date</label>
							@if(!$season->is_active || $market->started_at <= $now && $now < $market->finished_at)
								<input type="date" name="finished_at" id="market-finished_at" value="{{ empty($market->finished_at) ? '' : $market->finished_at->toDateString() }}" disabled="disabled" tabindex="2" class="form-control">
							@else
								<input type="date" name="finished_at" id="market-finished_at" value="{{ empty($market->finished_at) ? '' : $market->finished_at->toDateString() }}" tabindex="2" class="form-control">
							@endif
							@if($errors->has('finished_at'))
								<span class="help-block">
									<strong>{{ $errors->first('finished_at') }}</strong>
								</span>
							@endif
						</div>
					</div>
					@if($season->is_active && ($market->started_at > $now || $now >= $market->finished_at))
						<div class="row">
							<div class="col-sm-offset-6 col-sm-6">
								<div class="btn-group btn-group-justified">
									<div class="btn-group">
										<button type="submit" tabindex="3" class="btn btn-success">
											<i class="glyphicon glyphicon-floppy-disk"></i> Update Market
										</button>
									</div>
								</div>
							</div>
						</div>
					@endif
				</form>
			</div>
		</div>
	</div>
</div>
