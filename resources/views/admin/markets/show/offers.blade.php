<div class="row">
	<div id="market_offers-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#market_offers-panel" data-toggle="collapse" data-parent="market_offers-container">Offers</a>
				</h4>
			</div>
			<div id="market_offers-panel" class="panel-collapse collapse{{ count($offers) == 0 ? '' : ' in' }}">
				<div class="panel-body">
					<div class="table-responsive">
						<table id="offers-table" class="table table-hover offer-table">
							<thead>
								<tr>
									<th>Player</th>
									<th>Fantateam</th>
									<th>Team</th>
									<th>Position</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($offers as $idx => $offer)
									<tr>
										<!-- Offer Player -->
										<td class="table-text col-sm-3">
											<div>{{ $offer->player->name }}</div>
										</td>
										<!-- Offer Fantateam -->
										<td class="table-text col-sm-3">
											<div>{{ $offer->fantateam->name }}</div>
										</td>
										<!-- Offer Team -->
										<td class="table-text col-sm-3">
											<div>{{ $offer->player->team->name }}</div>
										</td>
										<!-- Offer Position -->
										<td class="table-text col-sm-2">
											<div>{{ $offer->player->position->abbreviation }}</div>
										</td>
										<td class="col-sm-1">
											<div class="btn-toolbar" role="toolbar">
												@if($season->is_active)
													<!-- Delete Button -->
													<div class="btn-group">
														<form action="{{ url('offers/'.$offer->id) }}" method="POST">
															{!! csrf_field() !!}
															{!! method_field('DELETE') !!}
															<button title="Remove Offer from Market" class="btn btn-xs btn-danger">
																<span class="glyphicon glyphicon-remove"></span>
															</button>
														</form>
													</div>
												@endif
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
