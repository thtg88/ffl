<div class="row">
	<div id="market_fantateams-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#market_fantateams-panel" data-toggle="collapse" data-parent="market_fantateams-container">FantaTeams</a>
				</h4>
			</div>
			<div id="market_fantateams-panel" class="panel-collapse collapse{{ count($rem_fantateams) <= 0 ? '' : ' in'}}">
				<div class="panel-body">
					@include('admin.markets.show.fantateams.list')
					@if($market->finished_at > $now)
						@include('admin.markets.show.fantateams.add')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
