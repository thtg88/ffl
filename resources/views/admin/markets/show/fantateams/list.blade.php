<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
			<table id="fantateams-table" class="table table-hover fantateam-table">
				<thead>
					<tr>
						<th>Fantateam</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($market_fantateams as $idx => $fantateam)
						<tr>
							<!-- FantaTeam Name -->
							<td class="table-text col-sm-10">
								<div>{{ $fantateam->name }}</div>
							</td>
							<td class="col-sm-2">
								<div class="btn-toolbar" role="toolbar">
									@if($market->finished_at > $now)
										<!-- Delete Button -->
										<div class="btn-group">
											<form action="{{ url('markets/'.$market->id.'/fantateams/'.$fantateam->id) }}" method="POST">
												{!! csrf_field() !!}
												{!! method_field('DELETE') !!}
												<button title="Remove FantatTeam from Market" class="btn btn-xs btn-danger">
													<span class="glyphicon glyphicon-remove"></span>
												</button>
											</form>
										</div>
									@endif
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
