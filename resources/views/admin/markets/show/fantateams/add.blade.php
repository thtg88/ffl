<div class="row">
	<div class="col-sm-12">
		<form method="POST" action="{{ url('markets/'.$market->id.'/fantateams') }}" class="form-horizontal" role="form">
			{!! csrf_field() !!}
			<div class="form-group">
				<!-- FantaTeam -->
				<div class="col-sm-offset-3 col-sm-3{{ $errors->has('fantateam_id') ? ' has-error' : '' }}">
					<label for="fantateam-id" class="control-label">FantaTeam</label>
					<select name="fantateam_id" id="fantateam-id" value="{{ old('fantateam_id') }}" tabindex="1" class="form-control">
						<option value="">Please select a FantaTeam...</option>
						@foreach($rem_fantateams as $fantateam)
							<option value="{{ $fantateam->id }}">{{ $fantateam->name }}</option>
						@endforeach
					</select>
					@if($errors->has('fantateam_id'))
						<span class="help-block">
							<strong>{{ $errors->first('fantateam_id') }}</strong>
						</span>
					@endif
				</div>
				<!-- Add FantaTeam -->
				<div class="col-sm-3">
					<div class="btn-group btn-group-justified">
						<div class="btn-group">
							<button type="submit" tabindex="9" class="btn btn-success">
								<i class="fa fa-plus"></i> Add FantaTeam
							</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
