@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>{{ $market->name }} Market - {{ $season->name }} Season</h1>
</div>
@if(!$season->is_active)
	@include('admin.seasons.alerts.ended')
@endif
@if(Session::has('market_finished'))
	@include('admin.markets.show.alerts.market-finished')
@elseif(Session::has('market_close_success'))
	@include('admin.markets.show.alerts.market-closed')
@elseif(Session::has('market_created_from_close'))
	@include('admin.markets.show.alerts.market-created-from-close')
@endif
@if(!$season->is_active || $market->started_at <= $now && $now < $market->finished_at)
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">Actions</h4>
			</div>
			<div class="panel-body">
				<form action="{{ url('markets/'.$market->id.'/close') }}" method="post" role="form" class="form-horizontal">
					{!! csrf_field() !!}
					{!! method_field('PATCH') !!}
					<div class="btn-group btn-group-justified">
						<div class="btn-group">
							<button type="submit" tabindex="3" class="btn btn-warning">
								<i class="glyphicon glyphicon-folder-close"></i> Close Market
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endif
@include('admin.markets.show.details')
@include('admin.markets.show.offers')
@include('admin.markets.show.fantateams.main')
@endsection
@section('scripts')
<script src="{{ asset('js/markets/single/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/markets/single/onload.js?v='.config('app.version')) }}"></script>
@endsection
