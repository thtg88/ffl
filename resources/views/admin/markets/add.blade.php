<div class="row">
	<div id="add_season_market-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#add_season_market-panel" data-toggle="collapse" data-parent="add_season_market-container">Add Market</a>
				</h4>
			</div>
			<div id="add_season_market-panel" class="panel-collapse collapse in">
				<div class="panel-body">
					<form method="POST" action="{{ url('markets') }}" class="form-horizontal" role="form">
						{!! csrf_field() !!}
						<input type="hidden" name="season_id" value="{{ $season->id }}">
						<div class="form-group">
							<!-- Name -->
							<div class="col-sm-4{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="season-name" class="control-label">Name</label>
								<input type="text" name="name" id="season-name" tabindex="1" value="{{ old('name') }}" class="form-control">
								@if($errors->has('name'))
									<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
								@endif
							</div>
							<!-- Start Date -->
							<div class="col-sm-4{{ $errors->has('started_at') ? ' has-error' : '' }}">
								<label for="season-started_at" class="control-label">Start Date</label>
								<input type="date" name="started_at" id="season-started_at" value="{{ old('started_at') }}" tabindex="2" class="form-control">
								@if($errors->has('started_at'))
									<span class="help-block">
										<strong>{{ $errors->first('started_at') }}</strong>
									</span>
								@endif
							</div>
							<!-- End Date -->
							<div class="col-sm-4{{ $errors->has('finished_at') ? ' has-error' : '' }}">
								<label for="season-finished_at" class="control-label">End Date</label>
								<input type="date" name="finished_at" id="season-finished_at" value="{{ old('finished_at') }}" tabindex="3" class="form-control">
								@if($errors->has('finished_at'))
									<span class="help-block">
										<strong>{{ $errors->first('finished_at') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<!-- End Date -->
							<div class="col-sm-4{{ $errors->has('fantateams') ? ' has-error' : '' }}">
								<label for="season-fantateams" class="control-label">Participating Fantateam</label>
								<select name="fantateams[]" id="season-fantateams" multiple tabindex="4" class="form-control">
									@foreach($fantateams as $fantateam)
										<option value="{{ $fantateam->id }}">{{ $fantateam->name }}</option>
									@endforeach
								</select>
								@if($errors->has('fantateams'))
									<span class="help-block">
										<strong>{{ $errors->first('fantateams') }}</strong>
									</span>
								@endif
							</div>
							<!-- Add Market -->
							<div class="col-sm-offset-4 col-sm-4">
								<div class="btn-group btn-group-justified">
									<div class="btn-group">
										<button type="submit" tabindex="5" class="btn btn-success">
											<i class="fa fa-plus"></i> Add Market
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
