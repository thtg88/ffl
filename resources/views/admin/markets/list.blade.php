<div class="row">
	<div id="season_markets-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#season_markets-panel" data-toggle="collapse" data-parent="season_markets-container">Markets</a>
				</h4>
			</div>
			<div id="season_markets-panel" class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="table-responsive">
						<table id="markets-table" class="table table-hover market-table">
							<thead>
								<tr>
									<th>Date Created</th>
									<th>Name</th>
									<th>Start Date</th>
									<th>Finish Date</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($markets as $idx => $market)
									<tr>
										<!-- Market Created At -->
										<td class="table-text col-sm-2">
											<div>{{ $market->created_at }}</div>
										</td>
										<!-- Market Name -->
										<td class="table-text col-sm-4">
											<div>{{ $market->name }}</div>
										</td>
										<!-- Market Started At -->
										<td class="table-text col-sm-2">
											<div>{{ $market->started_at }}</div>
										</td>
										<!-- Market Finished At -->
										<td class="table-text col-sm-2">
											<div>{{ $market->finished_at }}</div>
										</td>
										<td class="col-sm-2">
											<div class="btn-toolbar" role="toolbar">
												<!-- View Button -->
												<div class="btn-group">
													<a href="{{ url('admin/markets/'.$market->id) }}" title="View Market" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-folder-open"></span></a>
												</div>
												@if($season->is_active)
													<!-- Delete Button -->
													<div class="btn-group">
														<form action="{{ url('markets/'.$market->id) }}" method="POST">
															{!! csrf_field() !!}
															{!! method_field('DELETE') !!}
															<button title="Remove Market from Season" class="btn btn-xs btn-danger">
																<span class="glyphicon glyphicon-remove"></span>
															</button>
														</form>
													</div>
												@endif
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
