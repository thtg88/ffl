@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>{{ $season->name }} Season - Markets</h1>
</div>
@include('admin.markets.list')
@if($season->is_active)
	@include('admin.markets.add')
@else
	@include('admin.markets.alerts.ended')
@endif
@endsection
@section('scripts')
<script src="{{ asset('js/markets/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/markets/onload.js?v='.config('app.version')) }}"></script>
@endsection
