<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
			<table id="formations-table" class="table table-hover formation-table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Positions</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($formations as $idx => $formation)
						<tr>
							<!-- Formation Name -->
							<td class="table-text col-sm-2">
								<div>{{ $formation->name }}</div>
							</td>
							<!-- Formation Positions -->
							<td class="table-text col-sm-8">
								<div>{{ $formation->positions_compact }}</div>
							</td>
							<td class="col-sm-2">
								<div class="btn-toolbar" role="toolbar">
									@if($season->is_active)
										<!-- Delete Button -->
										<div class="btn-group">
											<form action="{{ url('seasons/'.$season->id.'/formations/'.$formation->id) }}" method="POST">
												{!! csrf_field() !!}
												{!! method_field('DELETE') !!}
												<button title="Remove Formation from Season" class="btn btn-xs btn-danger">
													<span class="glyphicon glyphicon-remove"></span>
												</button>
											</form>
										</div>
									@endif
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
