<div class="row">
	<div class="col-sm-12">
		<form method="POST" action="{{ url('seasons/'.$season->id.'/formations') }}" class="form-horizontal">
			{!! csrf_field() !!}
			<div class="form-group">
				<!-- Formation -->
				<div class="col-sm-offset-3 col-sm-3{{ $errors->has('formation') ? ' has-error' : '' }}">
					<label for="season-formation" class="control-label">Formation</label>
					<select name="formation_id" id="season-formation" tabindex="12" class="form-control">
						<option value="">Please select a Formation...</option>
						@foreach($all_formations as $formation)
							<option value="{{ $formation->id }}">{{ $formation->name }}</option>
						@endforeach
					</select>
					@if($errors->has('formation'))
						<span class="help-block">
							<strong>{{ $errors->first('formation') }}</strong>
						</span>
					@endif
				</div>
				<!-- Add Formation -->
				<div class="col-sm-3">
					<div class="btn-group btn-group-justified">
						<div class="btn-group">
							<button type="submit" tabindex="13" class="btn btn-success">
								<i class="fa fa-plus"></i> Add Formation
							</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
