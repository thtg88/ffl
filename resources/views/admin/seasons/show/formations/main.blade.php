<div class="row">
	<div id="season_formations-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#season_formations-panel" data-toggle="collapse" data-parent="season_formations-container">Formations</a>
				</h4>
			</div>
			<div id="season_formations-panel" class="panel-collapse collapse{{ count($all_formations) == 0 && $season->is_active ? '' : ' in' }}">
				<div class="panel-body">
					@include('admin.seasons.show.formations.list')
					@if($season->is_active)
						@include('admin.seasons.show.formations.add')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
