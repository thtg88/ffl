<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">{{ $season->name }} Season - Dashboard</h4>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="btn-toolbar" role="toolbar">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<a href="{{ url('admin/seasons/'.$season->id.'/markets') }}" tabindex="1" class="btn btn-lg btn-default"><span class="glyphicon glyphicon-usd"></span> Markets</a>
								</div>
								<div class="btn-group">
									<a href="{{ url('admin/seasons/'.$season->id.'/players') }}" tabindex="2" class="btn btn-lg btn-default"><span class="fa fa-male"></span> Players</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
