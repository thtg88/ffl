@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>{{ $season->name }} Season</h1>
</div>
@if(!$season->is_active)
	@include('admin.seasons.alerts.ended')
@endif
@include('admin.seasons.show.toolbar')
@include('admin.seasons.show.days')
@include('admin.seasons.show.fantateams.main')
@include('admin.seasons.show.users.main')
@include('admin.seasons.show.formations.main')
@endsection
@section('scripts')
<script src="{{ asset('js/seasons/single/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/seasons/single/onload.js?v='.config('app.version')) }}"></script>
@endsection
