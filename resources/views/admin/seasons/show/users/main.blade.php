<div class="row">
	<div id="season_users-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#season_users-panel" data-toggle="collapse" data-parent="season_users-container">Users</a>
				</h4>
			</div>
			<div id="season_users-panel" class="panel-collapse collapse{{ count($rem_users) == 0 && $season->is_active ? '' : ' in' }}">
				<div class="panel-body">
					@include('admin.seasons.show.users.list')
					@if($season->is_active)
						@include('admin.seasons.show.users.add')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
