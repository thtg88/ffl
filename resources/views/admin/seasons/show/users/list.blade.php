<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
			<table id="users-table" class="table table-hover user-table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($season_users as $idx => $user)
						<tr>
							<!-- User Name -->
							<td class="table-text col-sm-10">
								<div>{{ $user->name }}</div>
							</td>
							<td class="col-sm-2">
								<div class="btn-toolbar" role="toolbar">
									@if($season->is_active)
										<!-- Delete Button -->
										<div class="btn-group">
											<form action="{{ url('seasons/'.$season->id.'/users/'.$user->id) }}" method="POST">
												{!! csrf_field() !!}
												{!! method_field('DELETE') !!}
												<button title="Remove User from Season" class="btn btn-xs btn-danger">
													<span class="glyphicon glyphicon-remove"></span>
												</button>
											</form>
										</div>
									@endif
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
