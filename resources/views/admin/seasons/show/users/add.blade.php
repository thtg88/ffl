<div class="row">
	<div class="col-sm-12">
		<form method="POST" action="{{ url('seasons/'.$season->id.'/users') }}" class="form-horizontal">
			{!! csrf_field() !!}
			<div class="form-group">
				<!-- User -->
				<div class="col-sm-offset-3 col-sm-3{{ $errors->has('user_id') ? ' has-error' : '' }}">
					<label for="season-user" class="control-label">User</label>
					<select name="user_id" id="season-user" tabindex="10" class="form-control">
						<option value="">Please select a User...</option>
						@foreach($rem_users as $user)
							<option value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
					@if($errors->has('user_id'))
						<span class="help-block">
							<strong>{{ $errors->first('user_id') }}</strong>
						</span>
					@endif
				</div>
				<!-- Add User -->
				<div class="col-sm-3">
					<div class="btn-group btn-group-justified">
						<div class="btn-group">
							<button type="submit" tabindex="11" class="btn btn-success">
								<i class="fa fa-plus"></i> Add User
							</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
