<div class="row">
	<div id="season_days-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#season_days-panel" data-toggle="collapse" data-parent="season_days-container">Days</a>
				</h4>
			</div>
			<div id="season_days-panel" class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="table-responsive">
						<table id="days-table" class="table table-hover day-table">
							<thead>
								<tr>
									<th>Day Number</th>
									<th>Date Started</th>
									<th>Date Finished</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($days as $day)
									<tr>
										<!-- Day Number -->
										<td class="table-text col-sm-4">
											<div>{{ $day->day_number }}</div>
										</td>
										<!-- Day Started At -->
										<td class="table-text col-sm-2">
											<div>{{ $day->date_started }}</div>
										</td>
										<!-- Day Finished At -->
										<td class="table-text col-sm-2">
											<div>{{ $day->date_finished }}</div>
										</td>
										<td class="col-sm-2">
											<div class="btn-toolbar" role="toolbar">
												<!-- View Day Button -->
												<div class="btn-group">
													<a href="{{ url('admin/days/'.$day->id) }}" title="View Day" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-folder-open"></span></a>
												</div>
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
