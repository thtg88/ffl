<div class="row">
	<div class="col-sm-12">
		<form method="POST" action="{{ url('fantateams') }}" class="form-horizontal" role="form">
			{!! csrf_field() !!}
			<input type="hidden" name="season_id" value="{{ $season->id }}">
			<input type="hidden" name="budget" value="{{ $season->fantateam_budget }}">
			<div class="form-group">
				<!-- Name -->
				<div class="col-sm-3{{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="fantateam-name" class="control-label">Name</label>
					<input type="text" name="name" id="fantateam-name" value="{{ old('name') }}" tabindex="6" class="form-control">
					@if($errors->has('name'))
						<span class="help-block">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
					@endif
				</div>
				<!-- Owner -->
				<div class="col-sm-3{{ $errors->has('user_id') ? ' has-error' : '' }}">
					<label for="fantateam-user" class="control-label">Owner</label>
					<select name="user_id" id="fantateam-user" tabindex="7" class="form-control">
						<option value="">Please select the FantaTeam Owner...</option>
						@foreach($rem_season_users as $user)
							<option value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
					@if($errors->has('user_id'))
						<span class="help-block">
							<strong>{{ $errors->first('user_id') }}</strong>
						</span>
					@endif
				</div>
				<!-- Favourite Formation -->
				<div class="col-sm-3{{ $errors->has('formation_id') ? ' has-error' : '' }}">
					<label for="fantateam-formation" class="control-label">Favourite Formation</label>
					<select name="formation_id" id="fantateam-formation" tabindex="8" class="form-control">
						<option value="">Please select the FantaTeam Favourite Formation...</option>
						@foreach($formations as $formation)
							<option value="{{ $formation->id }}">{{ $formation->name }}</option>
						@endforeach
					</select>
					@if($errors->has('formation_id'))
						<span class="help-block">
							<strong>{{ $errors->first('formation_id') }}</strong>
						</span>
					@endif
				</div>
				<!-- Add FantaTeam -->
				<div class="col-sm-3">
					<div class="btn-group btn-group-justified">
						<div class="btn-group">
							<button type="submit" tabindex="9" class="btn btn-success">
								<i class="fa fa-plus"></i> Add FantaTeam
							</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
