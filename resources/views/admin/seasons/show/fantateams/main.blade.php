<div class="row">
	<div id="season_fantateams-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#season_fantateams-panel" data-toggle="collapse" data-parent="season_fantateams-container">FantaTeams</a>
				</h4>
			</div>
			<div id="season_fantateams-panel" class="panel-collapse collapse{{ (count($rem_season_users) == 0 && $season->is_active) ? '' : ' in' }}">
				<div class="panel-body">
					@include('admin.seasons.show.fantateams.list')
					@if($season->is_active)
						@include('admin.seasons.show.fantateams.add')
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
