<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
			<table id="fantateams-table" class="table table-hover fantateam-table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Owner</th>
						<th>Favourite Formation</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($fantateams as $idx => $fantateam)
						<tr>
							<!-- FantaTeam Name -->
							<td class="table-text col-sm-4">
								<div>{{ $fantateam->name }}</div>
							</td>
							<!-- FantaTeam Owner -->
							<td class="table-text col-sm-4">
								<div>{{ $fantateam->user->name }}</div>
							</td>
							<!-- FantaTeam Favourite Formation -->
							<td class="table-text col-sm-2">
								<div>{{ is_object($fantateam->formation) ? $fantateam->formation->name : '' }}</div>
							</td>
							<td class="col-sm-2">
								<div class="btn-toolbar" role="toolbar">
									<!-- Open Button -->
									<div class="btn-group">
										<a href="{{ url('fantateams/'.$fantateam->id) }}" title="View FantaTeam" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-folder-open"></span></a>
									</div>
									@if($season->is_active)
										<!-- Delete Button -->
										<div class="btn-group">
											<form action="{{ url('fantateams/'.$fantateam->id) }}" method="POST">
												{!! csrf_field() !!}
												{!! method_field('DELETE') !!}
												<button title="Delete FantaTeam" class="btn btn-xs btn-danger">
													<span class="glyphicon glyphicon-remove"></span>
												</button>
											</form>
										</div>
									@endif
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
