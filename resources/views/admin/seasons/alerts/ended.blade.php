<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-danger text-center" role="alert">
			<p>This Season is over. Edits are not possible.</p>
		</div>
	</div>
</div>
