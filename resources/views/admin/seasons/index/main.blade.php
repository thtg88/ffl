@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>Seasons</h1>
</div>
@include('admin.seasons.index.list')
@include('admin.seasons.index.add')
@endsection
@section('scripts')
<script src="{{ asset('js/seasons/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/seasons/onload.js?v='.config('app.version')) }}"></script>
@endsection
