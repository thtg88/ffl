<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">Seasons</h4>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table id="seasons-table" class="table table-hover season-table">
						<thead>
							<tr>
								<th>Season</th>
								<th>Current Day</th>
								<th>Date Started</th>
								<th>Date Finished</th>
								<th>Fantateams Budget</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($seasons as $season)
								<tr>
									<!-- Season Name -->
									<td class="table-text col-sm-2">
										<div>{{ $season->name }}</div>
									</td>
									<!-- Season Current Day -->
									<td class="table-text col-sm-2">
										<div>{{ $season->current_day == 0 ? '' : $season->current_day }}</div>
									</td>
									<!-- Season Started At -->
									<td class="table-text col-sm-2">
										<div>{{ $season->date_started->toDateString() }}</div>
									</td>
									<!-- Season Finished At -->
									<td class="table-text col-sm-2">
										<div>{{ $season->date_finished->toDateString() }}</div>
									</td>
									<!-- Season Fantateams Budget -->
									<td class="table-text col-sm-2">
										<div>{{ $season->fantateam_budget }}</div>
									</td>
									<td class="col-sm-2">
										<div class="btn-toolbar" role="toolbar">
											@if($season->is_active)
												<!-- Stop Season Button -->
												<div class="btn-group">
													<form action="{{ url('seasons/'.$season->id.'/stop') }}" method="POST">
														{!! csrf_field() !!}
														{!! method_field('PATCH') !!}
														<button title="Stop Season" class="btn btn-xs btn-warning">
															<span class="glyphicon glyphicon-minus-sign"></span>
														</button>
													</form>
												</div>
												@if($season->current_day != 0)
													<!-- Restart Season Button -->
													<div class="btn-group">
														<form action="{{ url('seasons/'.$season->id.'/restart') }}" method="POST">
															{!! csrf_field() !!}
															{!! method_field('PATCH') !!}
															<button title="Restart Season" class="btn btn-xs btn-warning">
																<span class="glyphicon glyphicon-fast-backward"></span>
															</button>
														</form>
													</div>
												@endif
												<!-- Next Day of Season Button -->
												<div class="btn-group">
													<form action="{{ url('seasons/'.$season->id.'/next_day') }}" method="POST">
														{!! csrf_field() !!}
														{!! method_field('PATCH') !!}
														<button title="Go to Next Day" class="btn btn-xs btn-default">
															<span class="glyphicon glyphicon-step-forward"></span>
														</button>
													</form>
												</div>
											@else
												<!-- Start Season Button -->
												<div class="btn-group">
													<form action="{{ url('seasons/'.$season->id.'/start') }}" method="POST">
														{!! csrf_field() !!}
														{!! method_field('PATCH') !!}
														<button title="Start Season" class="btn btn-xs btn-success">
															<span class="glyphicon glyphicon-triangle-right"></span>
														</button>
													</form>
												</div>
											@endif
											<!-- Open Season Button -->
											<div class="btn-group">
												<a href="{{ url('admin/seasons/'.$season->id) }}" title="Open Season" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-folder-open"></span></a>
											</div>
											<!-- Show Season Markets Button -->
											<div class="btn-group">
												<a href="{{ url('admin/seasons/'.$season->id.'/markets') }}" title="View Season Markets" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-usd"></span></a>
											</div>
											<!-- Show Season Players Button -->
											<div class="btn-group">
												<a href="{{ url('admin/seasons/'.$season->id.'/players') }}" title="View Season Players" class="btn btn-xs btn-default"><span class="fa fa-male"></span></a>
											</div>
											<!-- Delete Button -->
											<div class="btn-group">
												<form action="{{ url('seasons/'.$season->id) }}" method="POST">
													{!! csrf_field() !!}
													{!! method_field('DELETE') !!}
													<button title="DELETE Season" class="btn btn-xs btn-danger">
														<span class="glyphicon glyphicon-remove"></span>
													</button>
												</form>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
