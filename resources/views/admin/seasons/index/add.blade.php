<div class="row">
	<div id="add_season-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#add_season" data-toggle="collapse" data-parent="#add_season-container">New Season</a>
				</h4>
			</div>
			<div id="add_season" class="panel-collapse collapse{{ ($is_season_active && count($errors->default) == 0) ? '' : ' in' }}">
				<div class="panel-body">
					<!-- New Season Form -->
					<form action="{{ url('seasons') }}" method="POST" class="form-horizontal">
						{!! csrf_field() !!}
						<div class="form-group">
							<!-- Season Name -->
							<div class="col-sm-4{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="season-name" class="control-label">Name</label>
								<input type="text" name="name" id="season-name" value="{{ old('name') }}" tabindex="1" class="form-control">
								@if($errors->has('name'))
									<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
								@endif
							</div>
							<!-- Season Number of Days -->
							<div class="col-sm-4{{ $errors->has('number_days') ? ' has-error' : '' }}">
								<label for="season-number_days" class="control-label">Number of Days</label>
								<input type="text" name="number_days" id="season-number_days" value="{{ old('number_days') }}" tabindex="2" class="form-control">
								@if($errors->has('number_days'))
									<span class="help-block">
										<strong>{{ $errors->first('number_days') }}</strong>
									</span>
								@endif
							</div>
							<!-- Season Fantateams Budget -->
							<div class="col-sm-4{{ $errors->has('fantateam_budget') ? ' has-error' : '' }}">
								<label for="season-fantateam_budget" class="control-label">Fantateams Budget</label>
								<input type="text" name="fantateam_budget" id="season-fantateam_budget" value="{{ old('fantateam_budget') }}" tabindex="3" class="form-control">
								@if($errors->has('fantateam_budget'))
									<span class="help-block">
										<strong>{{ $errors->first('fantateam_budget') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<!-- Season Date Started -->
							<div class="col-sm-6{{ $errors->has('date_started') ? ' has-error' : '' }}">
								<label for="season-date_started" class="control-label">Date Started</label>
								<input type="date" name="date_started" id="season-date_started" value="{{ old('date_started') }}" tabindex="4" class="form-control">
								@if($errors->has('date_started'))
									<span class="help-block">
										<strong>{{ $errors->first('date_started') }}</strong>
									</span>
								@endif
							</div>
							<!-- Season Date Finished -->
							<div class="col-sm-6{{ $errors->has('date_finished') ? ' has-error' : '' }}">
								<label for="season-date_finished" class="control-label">Date Finished</label>
								<input type="date" name="date_finished" id="season-date_finished" value="{{ old('date_finished') }}" tabindex="5" class="form-control">
								@if($errors->has('date_finished'))
									<span class="help-block">
										<strong>{{ $errors->first('date_finished') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<!-- Season Formations -->
							<div class="col-sm-12{{ $errors->has('formations') ? ' has-error' : '' }}">
								<label for="season-formations" class="control-label">Formations</label>
								<select name="formations[]" id="season-formations" multiple tabindex="6" class="form-control">
								@foreach ($formations as $formation)
									<option value="{{ $formation->id }}">{{ $formation->name }}</option>
								@endforeach
								</select>
								@if($errors->has('formations'))
									<span class="help-block">
										<strong>{{ $errors->first('formations') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<!-- Positions -->
						<div class="form-group">
							@foreach ($positions as $idx => $position)
								<!-- Position Count -->
								<div class="{{ $positions_column_class }}{{ $errors->has('positions.'.$position->id.'.count') ? ' has-error' : '' }}">
									<label for="positions_{{ $position->id }}_count" class="control-label">Number of {{ $position->name }}s</label>
									<input type="text" name="positions[{{ $idx }}][count]" id="position_{{ $position->id }}_count" value="" tabindex="7" class="form-control">
									<input type="hidden" name="positions[{{ $idx }}][id]" id="position_{{ $position->id }}_id" value="{{ $position->id }}">
									@if($errors->has('positions.'.$position->id.'.count'))
										<span class="help-block">
											<strong>{{ $errors->first('positions.'.$position->id.'.count') }}</strong>
										</span>
									@endif
								</div>
							@endforeach
						</div>
						<div class="form-group">
							<!-- Season Users -->
							<div class="col-sm-12{{ $errors->has('users') ? ' has-error' : '' }}">
								<label for="season-users" class="control-label">Users</label>
								<select name="users[]" id="season-users" multiple tabindex="8" class="form-control">
								@foreach ($users as $user)
									<option value="{{ $user->id }}">{{ $user->name }}</option>
								@endforeach
								</select>
								@if($errors->has('users'))
									<span class="help-block">
										<strong>{{ $errors->first('users') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<!-- Add Season Button -->
						<div class="form-group">
							<div class="col-sm-offset-6 col-sm-6">
								<div class="btn-group btn-group-justified">
									<div class="btn-group">
										<button type="submit" tabindex="9" class="btn btn-success">
											<i class="fa fa-plus"></i> Add Season
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
