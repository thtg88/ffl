<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">Users</h4>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table id="users-table" class="table table-hover user-table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>Is Admin?</th>
								<th>Date Created</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $idx => $user)
								<tr>
									<!-- User Name -->
									<td class="table-text col-sm-4">
										<div>{{ $user->name }}</div>
									</td>
									<!-- User Email -->
									<td class="table-text col-sm-3">
										<div>{{ $user->email }}</div>
									</td>
									<!-- User Is Admin? -->
									<td class="table-text col-sm-1">
										<div>{{ $user->is_admin ? 'Yes' : 'No' }}</div>
									</td>
									<!-- User Created At -->
									<td class="table-text col-sm-2">
										<div>{{ $user->created_at }}</div>
									</td>
									<td class="col-sm-2">
										<div class="btn-toolbar" role="toolbar">
											<!-- View User Button -->
											<div class="btn-group">
												<a href="{{ url('admin/users/'.$user->id) }}" title="Edit User" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-folder-open"></span></a>
											</div>
											<!-- Delete Button -->
											<div class="btn-group">
												<form action="{{ url('users/'.$user->id) }}" method="POST">
													{!! csrf_field() !!}
													{!! method_field('DELETE') !!}
													<button title="DELETE User" class="btn btn-xs btn-danger">
														<span class="glyphicon glyphicon-remove"></span>
													</button>
												</form>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
