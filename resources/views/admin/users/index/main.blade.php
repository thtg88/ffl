@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>Users</h1>
</div>
@include('admin.users.index.list')
@include('admin.users.index.add')
@endsection
@section('scripts')
<script src="{{ asset('js/users/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/users/onload.js?v='.config('app.version')) }}"></script>
@endsection
