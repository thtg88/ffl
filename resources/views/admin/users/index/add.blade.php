<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">New User</h4>
			</div>
			<div class="panel-body">
				<!-- New User Form -->
				<form action="{{ url('users') }}" method="POST" class="form-horizontal">
					{!! csrf_field() !!}
					<div class="form-group">
						<!-- Name -->
						<div class="col-sm-4{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="user-name" class="control-label">Name</label>
							<input type="text" name="name" id="user-name" value="{{ old('name') }}" tabindex="1" class="form-control">
							@if($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
							@endif
						</div>
						<!-- Email -->
						<div class="col-sm-4{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="user-email" class="control-label">Email</label>
							<input type="email" name="email" id="user-email" value="{{ old('email') }}" tabindex="2" class="form-control">
							@if($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>
						<!-- Confirm Email -->
						<div class="col-sm-4{{ $errors->has('email_confirmation') ? ' has-error' : '' }}">
							<label for="user-email_confirmation" class="control-label">Confirm Email</label>
							<input type="email" name="email_confirmation" id="user-email_confirmation" tabindex="3" class="form-control">
							@if($errors->has('email_confirmation'))
								<span class="help-block">
									<strong>{{ $errors->first('email_confirmation') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<!-- Password -->
						<div class="col-sm-4{{ $errors->has('password') ? ' has-error' : '' }}">
							<label for="user-password" class="control-label">Password</label>
							<input type="password" name="password" id="user-password" tabindex="4" class="form-control">
							@if($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</div>
						<!-- Confirm Password -->
						<div class="col-sm-4{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
							<label for="user-password_confirmation" class="control-label">Confirm Password</label>
							<input type="password" name="password_confirmation" id="user-password_confirmation" tabindex="5" class="form-control">
							@if($errors->has('password_confirmation'))
								<span class="help-block">
									<strong>{{ $errors->first('password_confirmation') }}</strong>
								</span>
							@endif
						</div>
						<!-- Is Admin -->
						<div class="col-sm-4{{ $errors->has('is_admin') ? ' has-error' : '' }}">
							<label for="user-is_admin" class="control-label">Is Admin?</label>
							<select name="is_admin" id="user-is_admin" tabindex="6" class="form-control">
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
							@if($errors->has('is_admin'))
								<span class="help-block">
									<strong>{{ $errors->first('is_admin') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<!-- Add User Button -->
						<div class="col-sm-offset-8 col-sm-4">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<button type="submit" tabindex="7" class="btn btn-success">
										<i class="fa fa-plus"></i> Add User
									</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
