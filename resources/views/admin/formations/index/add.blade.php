<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">New Formation</h4>
			</div>
			<div class="panel-body">
				<!-- New Formation Form -->
				<form action="{{ url('formations') }}" method="POST" class="form-horizontal">
					{!! csrf_field() !!}
					<div class="form-group">
						<!-- Formation Name -->
						<div class="col-sm-12{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="formation-name" class="control-label">Name</label>
							<input type="text" name="name" id="formation-name" value="{{ old('name') }}" tabindex="1" class="form-control">
							@if($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<!-- Positions -->
					@foreach ($positions as $position)
						<div class="form-group">
							<!-- Position Name -->
							<div class="col-sm-6">
								<label for="position-name" class="control-label">Name</label>
								<input type="text" id="positions_{{ $position->id }}_name" class="form-control" value="{{ $position->name }}" disabled="disabled">
							</div>
							<!-- Position Abbreviation -->
							<div class="col-sm-3">
								<label for="position-abbreviation" class="control-label">Abbreviation</label>
								<input type="text" id="position_{{ $position->id }}_abbreviation" class="form-control" value="{{ $position->abbreviation }}" disabled="disabled">
							</div>
							<!-- Position Count -->
							<div class="col-sm-3{{ $errors->has('positions.'.$position->id.'.count') ? ' has-error' : '' }}">
								<label for="position-count" class="control-label">Count</label>
								@if($errors->has('positions.'.$position->id.'.count'))
									<input type="text" name="positions[{{ $position->id }}][count]" id="position_{{ $position->id }}_count" value="{{ old('positions[$position->id][count]') }}" tabindex="2" class="form-control">
									<span class="help-block">
										<strong>{{ $errors->first('positions.'.$position->id.'.count') }}</strong>
									</span>
								@else
									<input type="text" name="positions[{{ $position->id }}][count]" id="position_{{ $position->id }}_count" value="1" tabindex="2" class="form-control">
								@endif
							</div>
						</div>
					@endforeach
					<!-- Add Formation Button -->
					<div class="form-group">
						<div class="col-sm-offset-6 col-sm-6">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<button type="submit" tabindex="6" class="btn btn-success">
										<i class="fa fa-plus"></i> Add Formation
									</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
