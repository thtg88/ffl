@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>Formations</h1>
</div>
@include('admin.formations.index.list')
@include('admin.formations.index.add')
@endsection
@section('scripts')
<script src="{{ asset('js/formations/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/formations/onload.js?v='.config('app.version')) }}"></script>
@endsection
