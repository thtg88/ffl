<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">Formations</h4>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table id="formations-table" class="table table-hover formation-table">
						<thead>
							<tr>
								<th>Formation</th>
								<th>Positions</th>
								<th>Date Created</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($formations as $idx => $formation)
								<tr>
									<!-- Formation Name -->
									<td class="table-text col-sm-2">
										<div>{{ $formation->name }}</div>
									</td>
									<!-- Formation Positions -->
									<td class="table-text col-sm-6">
										<div>{{ $formation->positions_compact }}</div>
									</td>
									<!-- Formation Created At -->
									<td class="table-text col-sm-2">
										<div>{{ $formation->created_at }}</div>
									</td>
									<td class="col-sm-2">
										<div class="btn-toolbar" role="toolbar">
											<!-- Delete Button -->
											<div class="btn-group">
												<form action="{{ url('formations/'.$formation->id) }}" method="POST">
													{!! csrf_field() !!}
													{!! method_field('DELETE') !!}
													<button title="DELETE Formation" class="btn btn-xs btn-danger">
														<span class="glyphicon glyphicon-remove"></span>
													</button>
												</form>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
