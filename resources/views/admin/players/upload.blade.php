<div class="row">
	<div id="season_players_upload-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#season_players_upload-panel" data-toggle="collapse" data-parent="season_players_upload-container">Upload Players File</a>
				</h4>
			</div>
			<div id="season_players_upload-panel" class="panel-collapse collapse">
				<div class="panel-body">
					<form action="{{ url('unconfirmed_players/upload') }}" method="POST" enctype="multipart/form-data" class="form-horizontal" role="form">
						{!! csrf_field() !!}
						<input type="hidden" name="season_id" value="{{ $season->id }}">
						<div class="form-group">
							<div class="col-sm-4{{ $errors->has('file') ? ' has-error' : '' }}">
								<label for="season_players_file" class="control-label">Players File (.csv)</label>
								<input type="file" name="file" id="season_players_file" tabindex="1" class="form-control">
								@if($errors->has('file'))
									<span class="help-block">
										<strong>{{ $errors->first('file') }}</strong>
									</span>
								@endif
							</div>
							<div class="col-sm-4{{ $errors->has('csv_format') ? ' has-error' : '' }}">
								<label for="season_players_file_format" class="control-label">CSV Format</label>
								<select name="csv_format" id="season_players_file_format" tabindex="2" class="form-control">
									<option value="eu" selected="selected">European (semi-colon-separated, decimal comma)</option>
									<option value="uk">Anglo-Saxon (comma-separated, decimal point)</option>
								</select>
								@if($errors->has('csv_format'))
									<span class="help-block">
										<strong>{{ $errors->first('csv_format') }}</strong>
									</span>
								@endif
							</div>
							<div class="col-sm-4{{ $errors->has('contains_header') ? ' has-error' : '' }}">
								<label for="season_players_file_contains_header" class="control-label">Contains Header Row</label>
								<select name="contains_header" id="season_players_file" tabindex="3" class="form-control">
									<option value="1" selected="selected">Yes</option>
									<option value="0">No</option>
								</select>
								@if($errors->has('contains_header'))
									<span class="help-block">
										<strong>{{ $errors->first('contains_header') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-8 col-sm-4">
								<div class="btn-group btn-group-justified">
									<div class="btn-group">
										<button tabindex="4" class="btn btn-success">
											<span class="glyphicon glyphicon-cloud-upload"></span> Upload File
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
