<div class="row">
	<div id="season_players-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#season_players-panel" data-toggle="collapse" data-parent="season_players-container">Players</a>
				</h4>
			</div>
			<div id="season_players-panel" class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="table-responsive">
						<table id="players-table" class="table table-hover player-table">
							<thead>
								<tr>
									<th>Name</th>
									<th>Position</th>
									<th>Team</th>
									<th>Fantateam</th>
									<th>Cost</th>
									<th>Magic Avg</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($players as $idx => $player)
									<tr>
										<!-- Player Name -->
										<td class="table-text col-sm-2">
											<div>{{ $player->name }}</div>
										</td>
										<!-- Player Position -->
										<td class="table-text col-sm-1">
											<div>{{ $player->position->abbreviation }}</div>
										</td>
										<!-- Player Team -->
										<td class="table-text col-sm-2">
											<div>{{ $player->team->name }}</div>
										</td>
										<!-- Player Fantateam -->
										<td class="table-text col-sm-2">
											<div>{{ empty($player->fantateam) ? '' : $player->fantateam->name }}</div>
										</td>
										<!-- Player Cost -->
										<td class="table-text col-sm-1">
											<div>{{ $player->cost }}</div>
										</td>
										<!-- Player Magic Avg -->
										<td class="table-text col-sm-2">
											<div>{{ $player->magic_avg_percent }}</div>
										</td>
										<!-- Edit Button -->
										<td class="col-sm-2">
											<div class="btn-toolbar" role="toolbar">
												<div class="btn-group">
													<a href="{{ url('admin/players/'.$player->id) }}" title="View Player" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-folder-open"></span></a>
												</div>
												@if($season->is_active)
													<!-- Delete Button -->
													<div class="btn-group">
														<form action="{{ url('players/'.$player->id) }}" method="POST">
															{!! csrf_field() !!}
															{!! method_field('DELETE') !!}
															<button title="Delete Player" class="btn btn-xs btn-danger">
																<span class="glyphicon glyphicon-remove"></span>
															</button>
														</form>
													</div>
												@endif
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
