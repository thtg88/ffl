<div class="row">
	<div id="season_players_upload-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#season_players_upload-panel" data-toggle="collapse" data-parent="season_players_upload-container">Fetch Players from Gazzetta.it</a>
				</h4>
			</div>
			<div id="season_players_upload-panel" class="panel-collapse collapse{{ $errors->has('season_id') ? ' in' : '' }}">
				<div class="panel-body">
					@if($errors->has('season_id'))
						<div class="row">
							<div class="col-sm-12">
								<div class="alert alert-danger text-center">
									<p>{{ $errors->first('season_id') }}</p>
								</div>
							</div>
						</div>
					@endif
					<form action="{{ url('unconfirmed_players/fetch') }}" method="POST" enctype="multipart/form-data" class="form-horizontal" role="form">
						{!! csrf_field() !!}
						<input type="hidden" name="season_id" value="{{ $season->id }}">
						<div class="form-group">
							<div class="col-sm-4">
								<div class="btn-group btn-group-justified">
									<div class="btn-group">
										<button tabindex="1" class="btn btn-success">
											<span class="glyphicon glyphicon-cloud-upload"></span> Fetch
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
