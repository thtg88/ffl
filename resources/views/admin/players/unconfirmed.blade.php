<div class="row">
	<div id="season_players_unconfirmed-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#season_players_unconfirmed-panel" data-toggle="collapse" data-parent="season_players_unconfirmed-container">Unconfirmed Players</a>
				</h4>
			</div>
			<div id="season_players_unconfirmed-panel" class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="table-responsive">
						<table id="unfonfirmed_players-table" class="table table-hover unfonfirmed_player-table">
							<!-- Table Headings -->
							<thead>
								<tr>
									<th>Name</th>
									<th>Position</th>
									<th>Team</th>
									<th>Fantateam</th>
									<th>Cost</th>
									<th>Magic Avg</th>
									<th>Actions</th>
								</tr>
							</thead>
							<!-- Table Body -->
							<tbody>
								@foreach($unconfirmed_players as $idx => $player)
									<tr>
										<!-- Player Name -->
										<td class="table-text col-sm-2">
											<div>{{ $player->name }}</div>
										</td>
										<!-- Player Position -->
										<td class="table-text col-sm-1">
											<div>{{ $player->position->abbreviation }}</div>
										</td>
										<!-- Player Team -->
										<td class="table-text col-sm-2">
											<div>{{ $player->team->name }}</div>
										</td>
										<!-- Player Fantateam -->
										<td class="table-text col-sm-2">
											<div>{{ empty($player->fantateam) ? '' :$player->fantateam->name }}</div>
										</td>
										<!-- Player Cost -->
										<td class="table-text col-sm-1">
											<div>{{ $player->cost }}</div>
										</td>
										<!-- Player Magic Avg -->
										<td class="table-text col-sm-2">
											<div>{{ $player->magic_avg_percent / 100 }}</div>
										</td>
										<!-- Edit Button -->
										<td class="col-sm-2">
											<div class="btn-toolbar" role="toolbar">
												<!-- Confirm Button -->
												<div class="btn-group">
													<form action="{{ url('unconfirmed_players/'.$player->id.'/confirm') }}" method="POST" role="form">
														{!! csrf_field() !!}
														{!! method_field('PATCH') !!}
														<button title="Confirm Player" class="btn btn-xs btn-success">
															<span class="glyphicon glyphicon-send"></span>
														</button>
													</form>
												</div>
												<!-- Delete Button -->
												<div class="btn-group">
													<form action="{{ url('unconfirmed_players/'.$player->id) }}" method="POST" role="form">
														{!! csrf_field() !!}
														{!! method_field('DELETE') !!}
														<button title="Delete Player" class="btn btn-xs btn-danger">
															<span class="glyphicon glyphicon-remove"></span>
														</button>
													</form>
												</div>
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<form action="{{ url('seasons/'.$season->id.'/confirm_all_unconfirmed_players') }}" method="POST" role="form">
						{!! csrf_field() !!}
						{!! method_field('PATCH') !!}
						<div class="form-group">
							<div class="col-sm-offset-8 col-sm-4">
								<div class="btn-group btn-group-justified">
									<div class="btn-group">
										<button class="btn btn-success">Confirm All Players</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
