@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>{{ $season->name }} Season - Players</h1>
</div>
@if(!$season->is_active)
	@include('admin.seasons.alerts.ended')
@else
	@if(Session::has('fetch_error'))
		<div class="row">
			<div class="col-sm-12">
				<div class="alert alert-danger text-center">
					<p>{{ Session::get('fetch_error') }}</p>
				</div>
			</div>
		</div>
	@endif
	@include('admin.players.fetch')
	@include('admin.players.upload')
	@if(count($unconfirmed_players) > 0)
		@include('admin.players.unconfirmed')
	@endif
@endif
@include('admin.players.list')
@endsection
@section('scripts')
<script src="{{ asset('js/players/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/players/onload.js?v='.config('app.version')) }}"></script>
@endsection
