<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">New Position</h4>
			</div>
			<div class="panel-body">
				<!-- New Position Form -->
				<form action="{{ url('positions') }}" method="POST" class="form-horizontal">
					{!! csrf_field() !!}
					<div class="form-group">
						<!-- Position Name -->
						<div class="col-sm-4{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="position-name" class="control-label">Name</label>
							<input type="text" name="name" id="position-name" value="{{ old('name') }}" tabindex="1" class="form-control">
							@if($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
							@endif
						</div>
						<!-- Position Abbreviation -->
						<div class="col-sm-4{{ $errors->has('abbreviation') ? ' has-error' : '' }}">
							<label for="position-abbreviation" class="control-label">Abbreviation</label>
							<input type="text" name="abbreviation" id="position-abbreviation" value="{{ old('abbreviation') }}" tabindex="2" class="form-control">
							@if($errors->has('abbreviation'))
								<span class="help-block">
									<strong>{{ $errors->first('abbreviation') }}</strong>
								</span>
							@endif
						</div>
						<!-- Position Sequence -->
						<div class="col-sm-4{{ $errors->has('sequence') ? ' has-error' : '' }}">
							<label for="position-sequence" class="control-label">Sequence</label>
							<input type="text" name="sequence" id="position-sequence" value="{{ old('sequence') }}" tabindex="3" class="form-control">
							@if($errors->has('sequence'))
								<span class="help-block">
									<strong>{{ $errors->first('sequence') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<!-- Add Position Button -->
					<div class="form-group">
						<div class="col-sm-offset-8 col-sm-4">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<button type="submit" tabindex="4" class="btn btn-success">
										<i class="fa fa-plus"></i> Add Position
									</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
