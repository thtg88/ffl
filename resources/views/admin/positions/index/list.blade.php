<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">Positions</h4>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table id="positions-table" class="table table-hover position-table">
						<thead>
							<tr>
								<th>Sequence</th>
								<th>Position</th>
								<th>Abbreviation</th>
								<th>Date Created</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						@foreach($positions as $position)
							<tr>
								<!-- Position Sequence -->
								<td class="table-text col-sm-2">
									<div>{{ $position->sequence }}</div>
								</td>
								<!-- Position Name -->
								<td class="table-text col-sm-4">
									<div>{{ $position->name }}</div>
								</td>
								<!-- Position Abbreviation -->
								<td class="table-text col-sm-2">
									<div>{{ $position->abbreviation }}</div>
								</td>
								<!-- Position Created At -->
								<td class="table-text col-sm-2">
									<div>{{ $position->created_at }}</div>
								</td>
								<td class="col-sm-2">
									<div class="btn-toolbar" role="toolbar">
										<!-- Delete Button -->
										<div class="btn-group">
											<form action="{{ url('positions/'.$position->id) }}" method="POST">
												{!! csrf_field() !!}
												{!! method_field('DELETE') !!}
												<button title="DELETE Position" class="btn btn-xs btn-danger">
													<span class="glyphicon glyphicon-remove"></span>
												</button>
											</form>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
