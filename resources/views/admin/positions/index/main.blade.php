@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>Positions</h1>
</div>
@include('admin.positions.index.list')
@include('admin.positions.index.add')
@endsection
@section('scripts')
<script src="{{ asset('js/positions/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/positions/onload.js?v='.config('app.version')) }}"></script>
@endsection
