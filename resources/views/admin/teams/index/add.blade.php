<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">New Team</h4>
			</div>
			<div class="panel-body">
				<!-- New Team Form -->
				<form action="{{ url('teams') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="form-group">
						<!-- Team Name -->
						<div class="col-sm-4{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="team-name" class="control-label">Name</label>
							<input type="text" name="name" id="team-name" tabindex="1" class="form-control">
							@if($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
							@endif
						</div>
						<!-- Team Logo -->
						<div class="col-sm-4{{ $errors->has('logo') ? ' has-error' : '' }}">
							<label for="team-logo" class="control-label">Logo</label>
							<input type="file" name="logo" id="team-logo" tabindex="2" class="form-control">
							@if($errors->has('logo'))
								<span class="help-block">
									<strong>{{ $errors->first('logo') }}</strong>
								</span>
							@endif
						</div>
						<!-- Add Team Button -->
						<div class="col-sm-4">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<button tabindex="3" class="btn btn-success">
										<i class="fa fa-plus"></i> Add Team
									</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
