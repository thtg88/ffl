<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">Teams</h4>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table id="teams-table" class="table table-hover team-table">
						<thead>
							<tr>
								<th>Team</th>
								<th>Logo Path</th>
								<th>Date Created</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($teams as $team)
								<tr>
									<!-- Team Name -->
									<td class="table-text col-sm-4">
										<div>{{ $team->name }}</div>
									</td>
									<!-- Team Logo Path -->
									<td class="table-text col-sm-4">
										<div>{{ $team->logo_path }}</div>
									</td>
									<!-- Team Created At -->
									<td class="table-text col-sm-2">
										<div>{{ $team->created_at }}</div>
									</td>
									<td class="col-sm-2">
										<div class="btn-toolbar" role="toolbar">
											<div class="btn-group">
												<a href="{{ url('admin/teams/'.$team->id) }}" title="Show Team" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-folder-open"></span></a>
											</div>
											<!-- Delete Button -->
											<div class="btn-group">
												<form action="{{ url('teams/'.$team->id) }}" method="POST">
													{!! csrf_field() !!}
													{!! method_field('DELETE') !!}
													<button title="DELETE Team" class="btn btn-xs btn-danger">
														<span class="glyphicon glyphicon-remove"></span>
													</button>
												</form>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
