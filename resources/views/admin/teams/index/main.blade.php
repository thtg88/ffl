@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>Teams</h1>
</div>
@include('admin.teams.index.list')
@include('admin.teams.index.add')
@endsection
@section('scripts')
<script src="{{ asset('js/teams/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/teams/onload.js?v='.config('app.version')) }}"></script>
@endsection
