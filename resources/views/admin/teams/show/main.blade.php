@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>
		@if(!empty($team->name))
			<img src="{{ $team->logo_path }}" alt="{{ $team->name }}" style="width:50px;" class="img-responsive">
		@endif
		{{ $team->name }}
	</h1>
</div>
@include('admin.teams.show.players')
@endsection
@section('scripts')
<script src="{{ asset('js/teams/single/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/teams/single/onload.js?v='.config('app.version')) }}"></script>
@endsection
