<div class="row">
	<div id="team_players-container" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<h4 class="panel-title">
					<a href="#team_players-panel" data-toggle="collapse" data-parent="team_players-container">Players</a>
				</h4>
			</div>
			<div id="team_players-panel" class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="table-responsive">
						<table id="team_players-table" class="table table-hover team_player-table">
							<thead>
								<tr>
									<th>Player Name</th>
									<th>Season</th>
									<th>Games Played</th>
									<th>Goals Scored/Conceived</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($players as $player)
									<tr>
										<!-- Player Name -->
										<td class="table-text col-sm-4">
											<div>{{ $player->name }}</div>
										</td>
										<!-- Player Season -->
										<td class="table-text col-sm-2">
											<div>{{ $player->season->title }}</div>
										</td>
										<!-- Player Games Played -->
										<td class="table-text col-sm-2">
											<div>{{ $player->games_played }}</div>
										</td>
										<!-- Player Goals Scored/Conceived -->
										<td class="table-text col-sm-2">
											<div>{{ $player->goals_scored_conceived }}</div>
										</td>
										<td class="col-sm-2">
											<!-- View Player Button -->
											<div class="btn-toolbar" role="toolbar">
												<div class="btn-group">
													<a href="{{ url('admin/players/'.$player->id) }}" title="View Player" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-folder-open"></span></a>
												</div>
											</div>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
