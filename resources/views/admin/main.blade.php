@extends('layouts.app.main')
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Admin Dashboard</h4>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="btn-toolbar" role="toolbar">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<a href="{{ url('admin/seasons') }}" tabindex="1" class="btn btn-lg btn-default"><span class="glyphicon glyphicon-calendar"></span> Seasons</a>
								</div>
								<div class="btn-group">
									<a href="{{ url('admin/users') }}" tabindex="2" class="btn btn-lg btn-default"><span class="glyphicon glyphicon-user"></span> Users</a>
								</div>
								<div class="btn-group">
									<a href="{{ url('admin/teams') }}" tabindex="3" class="btn btn-lg btn-default"><span class="fa fa-users"></span> Teams</a>
								</div>
								<div class="btn-group">
									<a href="{{ url('admin/formations') }}" tabindex="4" class="btn btn-lg btn-default"><span class="glyphicon glyphicon-equalizer"></span> Formations</a>
								</div>
								<div class="btn-group">
									<a href="{{ url('admin/positions') }}" tabindex="5" class="btn btn-lg btn-default"><span class="glyphicon glyphicon-pawn"></span> Positions</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
