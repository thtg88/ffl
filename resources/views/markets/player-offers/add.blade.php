<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Add Offer</h4>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-12">
						<form method="POST" action="{{ url('offers') }}" class="form-horizontal" role="form">
							{!! csrf_field() !!}
							<input type="hidden" name="fantateam_id" id="offer-fantateam_id" value="{{ $fantateam->id }}">
							<input type="hidden" name="market_id" value="{{ $market->id }}">
							<input type="hidden" name="player_id" id="offer-player_id" value="{{ $player->id }}">
							<div class="form-group">
								<!-- Amount Offered -->
								<div class="col-sm-6{{ $errors->has('amount_offered') || $errors->has('fantateam_id') || $errors->has('player_id') ? ' has-error' : '' }}">
									<label for="offer-amount_offered" class="control-label">Amount</label>
									<input type="number" name="amount_offered" id="offer-amount_offered" value="{{ old('amount_offered') }}" tabindex="1" class="form-control">
									@if($errors->has('amount_offered'))
										<span class="help-block">
											<strong>{{ $errors->first('amount_offered') }}</strong>
										</span>
									@elseif($errors->has('fantateam_id'))
										<span class="help-block">
											<strong>{{ $errors->first('fantateam_id') }}</strong>
										</span>
									@elseif($errors->has('player_id'))
										<span class="help-block">
											<strong>{{ $errors->first('player_id') }}</strong>
										</span>
									@endif
								</div>
								<!-- Add Offer -->
								<div class="col-sm-6">
									<div class="btn-group btn-group-justified">
										<div class="btn-group">
											<button type="submit" tabindex="2" class="btn btn-success">
												<i class="fa fa-plus"></i> Offer
											</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
