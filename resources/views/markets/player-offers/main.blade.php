@extends('layouts.app.main')
@section('content')
<div class="page-header">
	<h1>{{ $player->name }} - {{ $market->name }} Market - {{ $season->name }} Season</h1>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-info">
			<p>The max spend per player is: {{ $max_spend_per_player }}</p>
			<p>Your current budget is: {{ $fantateam->budget }}</p>
		</div>
	</div>
</div>
@include('markets.player-offers.list')
@include('markets.player-offers.add')
@endsection
@section('scripts')
<script src="{{ asset('js/markets/player-offers/dataTable.js?v='.config('app.version')) }}"></script>
<script src="{{ asset('js/markets/player-offers/onload.js?v='.config('app.version')) }}"></script>
@endsection
