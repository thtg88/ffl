<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Offers</h4>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="table-responsive">
							<table id="player_offers-table" class="table table-hover">
								<thead>
									<tr>
										<th>Amount</th>
										<th>Offered At</th>
										<th>Fantateam</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@foreach($offers as $idx => $offer)
										<tr class="">
											<!-- Offer Amount -->
											<td class="table-text col-sm-2">
												<div>{{ $offer->amount_offered }}</div>
											</td>
											<!-- Offer Amount -->
											<td class="table-text col-sm-4">
												<div>{{ $offer->created_at }}</div>
											</td>
											<!-- Offer Fantateam -->
											<td class="table-text col-sm-4">
												<div>{{ $offer->fantateam->name }}</div>
											</td>
											<td class="col-sm-2">
												<div class="btn-toolbar" role="toolbar">
													<!-- View Fantateam -->
													<div class="btn-group">
														<a href="{{ url('fantateams/'.$offer->fantateam->id) }}" title="View Fantateam" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-folder-open"></span></a>
													</div>
												</div>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
