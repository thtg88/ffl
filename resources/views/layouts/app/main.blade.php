<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>The FFL</title>

		<!-- Fonts -->
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">

		<!-- Styles -->
		<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/all.css')) }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css?v='.config('app.version')) }}">
		@yield('styles')
	</head>
	<body id="app-layout">
		@include('layouts.app.nav')
		<div class="container">
			@if(isset($errors) && $errors->any())
				<div class="row">
					<div class="col-sm-12">
						<div class="alert alert-danger">
							<p>There were problems with the values you submitted.</p>
						</div>
					</div>
				</div>
			@endif
			<!-- Main Content -->
			@yield('content')
		</div>
		<footer>
			<div class="container">
				<div class="well text-right">
					<p>&copy; The FFL - Version {{ config('app.version') }}</p>
				</div>
			</div>
		</footer>
		<!-- Scripts -->
		<script src="{{ asset(elixir('js/app.js')) }}"></script>
		@yield('scripts')
	</body>
</html>
