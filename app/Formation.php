<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
	protected $fillable = [
		'name',
	];
	protected $appends = [
		'positions_compact',
	];

	public function getPositionsCompactAttribute()
	{
		$positions = [];
		foreach ($this->positions as $pos)
		{
			$positions[] = $pos->abbreviation;
		}
		return implode(", ", $positions);
	}

	public function positions()
	{
		return $this->belongsToMany('App\Position', 'formation_position');
	}

	public function fantateams()
	{
		return $this->hasMany('App\Fantateam');
	}

	public function seasons()
	{
		return $this->belongsToMany('App\Season', 'season_formation');
	}
}
