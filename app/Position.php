<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
	protected $fillable = [
		'abbreviation',
		'name',
		'sequence',
	];

	public function formations()
	{
		return $this->belongsToMany('App\Formation', 'season_formation');
	}

	// How many per each position should be allowed in a fantateam in a season
	public function seasons()
	{
		return $this->belongsToMany('App\Season', 'season_position');
	}

	// How many per each position should be allowed in a market for a fantateam in a season
	public function markets()
	{
		return $this->belongsToMany('App\Market', 'market_position');
	}

	public function players()
	{
		return $this->hasMany('App\Player');
	}
}
