<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Offer;
use App\User;

class OfferPolicy
{
	use HandlesAuthorization;

	public function before($user, $ability)
	{
		if($user->is_admin || $ability === "store")
		{
			return TRUE;
		}
	}

	public function edit(User $user, Offer $offer)
	{
		return $user->id == $offer->fantateam->user_id;
	}

	public function destroy(User $user, Offer $offer)
	{
		return $user->id == $offer->fantateam->user_id;
	}
}
