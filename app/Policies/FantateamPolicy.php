<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;
use App\Fantateam;

class FantateamPolicy
{
	use HandlesAuthorization;

	public function before($user, $ability)
	{
		if($user->is_admin)
		{
			return TRUE;
		}
	}

	public function edit(User $user, Fantateam $fantateam)
	{
		return $user->id == $fantateam->user_id;
	}

	public function destroy(User $user, Fantateam $fantateam)
	{
		return $user->id == $fantateam->user_id;
	}

	public function addOffer(User $user, Fantateam $fantateam)
	{
		return $user->id == $fantateam->user_id;
	}
}
