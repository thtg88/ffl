<?php

namespace App\Repositories;

use App\Fantateam;

class FantateamRepository extends BaseRepository
{
  /**
   * Create a new repository instance.
   *
   * @param \App\Fantateam
   * @return  void
   */
  public function __construct(Fantateam $fantateam)
  {
    $this->model = $fantateam;
  }

  /**
   * Returns the fantateam from a given id.
   *
   * @param int $id The fantateam id.
   * @return  \App\Fantateam
   */
  public function find($id)
  {
    return $this->model
			->with('season')
      ->find($id);
  }

  /**
   * Returns the fantateams from a given user id.
   *
   * @param int $user_id  The user id.
   * @return  Collection
   */
  public function getUserFantateams($user_id)
  {
    return $this->model
			->with('season')
      ->where('user_id', $user_id)
      ->get();
  }

  /**
   * Returns the fantateam from a given user and season id.
   *
   * @param int $user_id  The user id.
   * @param int $season_id  The season id.
   * @return  \App\Fantateam
   */
  public function getUserFantateam($user_id, $season_id)
  {
    return $this->model
			->with('season')
      ->where('user_id', $user_id)
      ->where('season_id', $season_id)
      ->first();
  }
}
