<?php

namespace App\Repositories;

use App\Offer;

class OfferRepository extends BaseRepository
{
  /**
   * Create a new repository instance.
   *
   * @param \App\Offer
   * @return  void
   */
  public function __construct(Offer $offer)
  {
    $this->model = $offer;
  }

  /**
   * Return the offer made in a given market for a given player.
   *
   * @param int $market_id  The id of the market.
   * @param int $player_id  The id of the player.
   * @return  Collection
   */
  public function getPlayerOffers($market_id, $player_id)
  {
    return $this->model
      ->where([
        ['market_id', $market_id],
        ['player_id', $player_id],
      ])
      ->orderBy('amount_offered', 'desc')
      ->orderBy('id')
      ->get();
  }

  /**
   * Returns the offer made in a given market not belonging to a certain fantateam.
   *
   * @param int $market_id  The id of the market.
   * @param int $excluded_fantateam_id The id of the fantateam to exclude from the results.
   * @return  Collection
   */
  public function getOtherFantateamsOffers($market_id, $excluded_fantateam_id)
  {
    return $this->model
      ->where([
        ['market_id', $market_id],
        ['fantateam_id', '<>', $excluded_fantateam_id],
      ])
      ->orderBy('player_id')
      ->orderBy('amount_offered')
      ->orderBy('id')
      ->get();
  }

  /**
   * Return the offers from a given market and fantateam.
   *
   * @param int $market_id The id of the market.
   * @param int $fantateam_id The id of the fantateam.
   * @return  Collection
   */
  public function getFantateamOffers($market_id, $fantateam_id)
  {
    return $this->model
      ->where([
        ['market_id', $market_id],
        ['fantateam_id', $fantateam_id],
      ])
      ->get();
  }

  /**
   * Return the maximum offer for every player from a given market id.
   *
   * @param int $market_id  The id of the market.
   * @return  Collection
   */
  public function getMarketMaxOffers($market_id)
  {
    return $this->model
			->select(\DB::raw('max(amount_offered) as amount, player_id'))
          ->where('market_id', $market_id)
          ->groupBy('player_id')
          ->get();
  }

  /**
   * Return a single offer made for a given player from a given fantateam
   * during a given market.
   *
   * @param int $market_id  The id of the market.
   * @param int $fantateam_id The id of the fantateam.
   * @param int $player_id  The id of the player.
   * @return  \App\Offer
   */
  public function getByMarketFantateamAndPlayer($market_id, $fantateam_id, $player_id)
  {
    return $this->model
			->where([
        ['market_id', $market_id],
        ['fantateam_id', $fantateam_id],
        ['player_id', $player_id],
      ])
      ->first();
  }
}
