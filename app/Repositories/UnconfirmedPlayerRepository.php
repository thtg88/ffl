<?php

namespace App\Repositories;

use App\UnconfirmedPlayer;
use App\Player;

class UnconfirmedPlayerRepository extends BaseRepository
{
  /**
   * Create a new repository instance.
   *
   * @param \App\UnconfirmedPlayer
   * @return  void
   */
  public function __construct(UnconfirmedPlayer $unconfirmed_player)
  {
    $this->model = $unconfirmed_player;
  }

  /**
   * Return the unconfirmed players from a given season id.
   *
   * @param int $season_id  The id of the season.
   * @return  Collection
   */
  public function getSeasonUnconfirmedPlayers($season_id)
  {
    return $this->model
      ->where('season_id', $season_id)
      ->get();
  }

  /**
   * Return an unconfirmed player from a given name, position id, and season id.
   *
   * @param int $season_id  The id of the season.
   * @param string $name  The name of the unconfirmed player.
   * @param int $position_id  The id of the position.
   * @return  UnconfirmedPlayer
   */
  public function getByNamePositionAndSeason($season_id, $name, $position_id)
  {
    return $this->model
      ->where('season_id', $season_id)
      ->where('name', $name)
      ->where('position_id', $position_id)
      ->first();
  }

  /**
   * Confirm an unconfirmed player instance from a given id.
   *
   * @param int The unconfirmed player id.
   * @return  int|null
   * @TODO improve
   */
  public function confirm($id)
  {
    $unconfirmed_player = $this->find($id);
    if($unconfirmed_player === null)
    {
      return null;
    }

    $except = [
      $unconfirmed_player->getKeyName(),
      $unconfirmed_player->getCreatedAtColumn(),
      $unconfirmed_player->getUpdatedAtColumn(),
    ];

    $attributes = array_except($unconfirmed_player->getAttributes(), $except);

    $new_player = new Player;
    foreach($attributes as $attribute => $value)
    {
      $new_player->{$attribute} = $unconfirmed_player->{$attribute};
    }
    $new_player->save();

    return $this->destroy($unconfirmed_player->id);
  }

  /**
   * Confirm all the unconfirmed player instances from a given season id.
   *
   * @param int The season id.
   * @return array The list of player ids which confirmation failed.
   */
  public function confirmAll($season_id)
  {
    // Get all unconfirmed players
    $unconfirmed_players = $this->getSeasonUnconfirmedPlayers($season_id);

    $failed_confirm_player_ids = [];
		foreach($unconfirmed_players as $unconfirmed_player)
    {
      $response = $this->confirm($unconfirmed_player->id);
      if($response === null)
      {
        $failed_confirm_player_ids[] = $unconfirmed_player->id;
      }
		}
    return $failed_confirm_player_ids;
  }
}
