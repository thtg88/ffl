<?php

namespace App\Repositories;

use App\Team;

class TeamRepository extends BaseRepository
{
  /**
   * Create a new repository instance.
   *
   * @param \App\Team
   * @return  void
   */
  public function __construct(Team $team)
  {
    $this->model = $team;
  }

  /**
   * Return a team from a given name.
   *
   * @param string  $name The name of the team.
   * @return  \App\Team
   */
  public function getByName($name)
  {
    return $this->model
      ->where('name', $name)
      ->first();
  }

  /**
   * Return all the teams, sorted by name.
   *
   * @return  Collection
   */
  public function all()
  {
    return $this->model
      ->orderBy('name')
      ->get();
  }
}
