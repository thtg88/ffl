<?php

namespace App\Repositories;

use App\Market;

class MarketRepository extends BaseRepository
{
  /**
   * Create a new repository instance.
   *
   * @param \App\Market
   * @return  void
   */
  public function __construct(Market $market)
  {
    $this->model = $market;
  }

  /**
   * Returns the finished markets from a given fantateam id.
   *
   * @param int $fantateam_id The id of the fantateam.
   * @return  Collection
   */
  public function getFantateamFinishedMarkets($fantateam_id)
  {
    return $this->model
			->select('markets.*')
      ->join('market_fantateam', 'markets.id', '=', 'market_fantateam.market_id')
      ->where('market_fantateam.fantateam_id', $fantateam_id)
      ->where('finished_at', '<', config('app.now')->toDateTimeString())
      ->get();
  }

  /**
   * Returns the next market for a given fantateam id.
   *
   * @param int $fantateam_id The id of the fantateam.
   * @return \App\Market
   */
  public function getNext($fantateam_id)
  {
    return $this->model
			->select('markets.*')
      ->join('market_fantateam', 'markets.id', '=', 'market_fantateam.market_id')
      ->where('market_fantateam.fantateam_id', $fantateam_id)
      ->where('finished_at', '>', config('app.now')->toDateTimeString())
      ->orderBy('finished_at')
      ->first();
  }

  /**
   * Returns all the markets for a given season id.
   *
   * @param int $season_id  The id of the season.
   * @return  Collection
   */
  public function getSeasonMarkets($season_id)
  {
    return $this->model
			->where('season_id', $season_id)
      ->orderBy('created_at', 'desc')
      ->get();
  }

  /**
   * Returns a new market instance from the given data.
   *
   * @param array $data
   * @return Market
   */
  public function create($data)
  {
    // Create
    $market = parent::create($data);

    // Attach fantateams to market
    foreach($data['fantateams'] as $fantateam_id)
    {
      $market->fantateams()->attach($fantateam_id);
    }

    return $market;
  }
}
