<?php

namespace App\Repositories;

use App\Day;

class DayRepository extends BaseRepository
{
  /**
   * Create a new repository instance.
   *
   * @param \App\Day
   * @return  void
   */
  public function __construct(Day $day)
  {
    $this->model = $day;
  }
}
