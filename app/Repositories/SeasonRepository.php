<?php

namespace App\Repositories;

//use App\Repositories\DayRepository;
use App\Season;

class SeasonRepository extends BaseRepository
{
  /**
   * The day repository implementation.
   *
   * @var \App\Repositories\DayRepository
   */
  protected $days;

  /**
   * Create a new repository instance.
   *
   * @param \App\Season
   * @param \App\Repositories\DayRepository
   * @return  void
   */
  public function __construct(Season $season, DayRepository $days)
  {
    $this->model = $season;
    $this->days = $days;
  }

  /**
   * Returns all the seasons.
   *
   * @return Collection
   */
  public function all()
  {
    return $this->model
      ->orderBy('created_at', 'asc')
      ->get();
  }

  /**
   * Returns the active season for a certain user from a given user id.
   *
   * @param int $user_id  The id of the user.
   * @return  \App\Season
   */
  public function getUserActiveSeason($user_id)
  {
    return $this->model
      ->select('seasons.*')
      ->join('user_season', 'seasons.id', '=', 'user_season.season_id')
      ->where('user_id', $user_id)
      ->where('is_active', 1)
      ->orderBy('date_started', 'desc')
			->first();
  }

  /**
   * Returns the ended seasons for a certain user from a given user id.
   *
   * @param int $user_id  The id of the user.
   * @return  Collection
   */
  public function getUserEndedSeasons($user_id)
  {
    return $this->model
      ->select('seasons.*')
      ->join('user_season', 'seasons.id', '=', 'user_season.season_id')
      ->where('user_id', $user_id)
      ->where('is_active', 0)
      ->orderBy('date_started', 'desc')
			->get();
  }

  /**
   * Returns a new Season instance from the given data.
   *
   * @param array $data
   * @return \App\Season
   */
  public function create($data)
  {
    // Create
    $season = parent::create($data);

    // Create days
		for($i = 1; $i <= $season->number_days; $i++)
    {
      $this->days->create([
        'season_id' => $season->id,
        'day_number' => $i,
      ]);
    }

		foreach($data['formations'] as $idx => $id)
    {
      // Attach formations
			$season->formations()->attach($id);
    }

		foreach($data['positions'] as $idx => $pos)
    {
			for($i = 0; $i < $pos['count']; $i++)
      {
        // Attach positions
        $season->positions()->attach($pos['id']);
    	}
		}

		foreach($data['users'] as $idx => $id)
    {
      // Attach users
			$season->users()->attach($id);
		}

    return $season;
  }

  /**
   * Updates all seasons to be not active (stop).
   *
   * @return  bool
   */
  public function stopAll()
  {
    return $this->model
      ->where('is_active', 1)
      ->update(['is_active' => 0]);
  }
}
