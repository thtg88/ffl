<?php

namespace App\Repositories;

class BaseRepository implements BaseRepositoryInterface
{
  /**
   * The repository model.
   *
   * @var \Illuminate\Eloquent\Model
   */
  protected $model;

  /**
   * Return all the model instances.
   *
   * @return  Collection
   */
  public function all()
  {
    return $this->model->all();
  }

  /**
   * Return a model from a given id.
   *
   * @param int $id The id of the player.
   * @return  \Illuminate\Eloquent\Model
   */
  public function find($id)
  {
    return $this->model->find($id);
  }

  /**
   * Returns a new model instance from the given data.
   *
   * @param array $data
   * @return \Illuminate\Eloquent\Model
   */
  public function create($data)
  {
    return $this->model->create($data);
  }

  /**
   * Deletes a model instance from a given id.
   *
   * @param int $id The id of the model.
   * @return int
   */
  public function destroy($id)
  {
    return $this->model->destroy($id);
  }

  /**
   * Updates a model instance with given data, from a given id.
   *
   * @param int $id The id of the model.
   * @param array $data
   * @return bool|null
   */
  public function update($id, $data)
  {
    // Get model
    $model = $this->find($id);
    if($model === null)
    {
      return null;
    }

    if(!isset($data['updated_at']))
    {
      // Set updated timestamp
      $data['updated_at'] = config('app.now')->toDateTimeString().'.000';
    }

    // Update
    $model->fill($data)->save();

    return $model;
  }
}
