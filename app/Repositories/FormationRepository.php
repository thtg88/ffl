<?php

namespace App\Repositories;

use App\Formation;

class FormationRepository
{
  /**
   * Create a new repository instance.
   *
   * @param \App\Formation
   * @return  void
   */
  public function __construct(Formation $formation)
  {
    $this->model = $formation;
  }

  /**
   * Return all the formations.
   *
   * @return Collection
   */
  public function all()
  {
    return $this->model
			->with('positions')
      ->orderBy('name', 'asc')
      ->get();
  }

  /**
   * Return a new resource instance from the given data.
   *
   * @param array
   * @return \App\Formation
   */
  public function create($data)
  {
    // Formation
		$formation = parent::create($data);

		foreach($data['positions'] as $key => $position)
    {
      for($i = 0; $i < $position['count']; $i++)
      {
        // Attach positions
        $formation->positions()->attach($key);
      }
    }
    return $formation;
  }
}
