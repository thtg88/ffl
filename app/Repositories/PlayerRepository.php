<?php

namespace App\Repositories;

use App\Player;

class PlayerRepository extends BaseRepository
{
  /**
   * Create a new repository instance.
   *
   * @param \App\Player
   * @return  void
   */
  public function __construct(Player $player)
  {
    $this->model = $player;
  }

  /**
   * Return the players from a given season id.
   *
   * @param int $season_id  The id of the season.
   * @return  Collection
   */
  public function getSeasonPlayers($season_id)
  {
    return $this->model
      ->where('season_id', $season_id)
      ->get();
  }

  /**
   * Returns the players from a given team id.
   *
   * @param int $team_id  The id of the team.
   * @return  Collection
   */
  public function getTeamPlayers($team_id)
  {
    return $this->model
      ->where('team_id', $team_id)
      ->orderBy('name')
      ->get();
  }

  /**
   * Returns a player from a given name, season id, and position id.
   *
   * @param int $season_id  The id of the season
   * @param string  $name The name of the player.
   * @param int $position_id  The id of the position.
   * @return  \App\Player
   */
  public function getByNamePositionAndSeason($season_id, $name, $position_id)
  {
    return $this->model
      ->where([
        'season_id' => $season_id,
        'name' => $name,
        'position_id' => $position_id,
      ])
      ->first();
  }

  /**
   * Return the available players from a given season id, excluding those in
   * given positions and given ids.
   *
   * @param int $season_id  The season id.
   * @param array  $excluded_player_ids An array containing ids of players to exclude.
   * @param array  $excluded_position_ids An array containing ids of positions to exclude.
   * @return  Collection
   */
  public function getAvailablePlayers($season_id, array $excluded_player_ids, array $excluded_position_ids)
  {
    // Get all season players
    $available_players = $this->model->where('season_id', $season_id);

    if(count($excluded_player_ids) > 0)
    {
      // Exclude given player ids
      $available_players = $available_players->whereNotIn('id', $excluded_player_ids);
    }

    if(count($excluded_position_ids) > 0)
    {
      // Exclude given position ids
      $available_players = $available_players->whereNotIn('position_id', $excluded_position_ids);
    }

    return $available_players->orderBy('position_id')
      ->orderBy('team_id')
      ->orderBy('name')
      ->get();
  }

  /**
   * Returns the count of players for each position currently owned by a given
   * fantateam.
   *
   * @param int $fantateam_id The id of the fantateam.
   * @return  Collection
   */
  public function getFantateamPlayersPositionCount($fantateam_id)
  {
    return $this->model
      ->select(\DB::raw('count(*) as pos_count, position_id'))
      ->groupBy('position_id')
      ->where('fantateam_id', $fantateam_id)
      ->get();
  }
}
