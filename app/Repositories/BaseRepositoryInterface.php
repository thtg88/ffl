<?php

namespace App\Repositories;

interface BaseRepositoryInterface
{
  public function all();
  
  public function create($data);

  public function destroy($id);

  public function find($id);

  public function update($id, $data);
}
