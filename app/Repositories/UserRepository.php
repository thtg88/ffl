<?php

namespace App\Repositories;

use App\User;

class UserRepository extends BaseRepository
{
  /**
   * Create a new repository instance.
   *
   * @param \App\User
   * @return  void
   */
  public function __construct(User $user)
  {
    $this->model = $user;
  }

  /**
   * Returns all the users.
   *
   * @return Collection
   */
  public function all()
  {
    return $this->model
      ->orderBy('name')
      ->get();
  }

  /**
   * Returns a newly-created user instance from the given data.
   *
   * @param array $data
   * @return User
   */
  public function create($data)
  {
    $data['password'] = bcrypt($data['password']);
    return parent::create($data);
  }
}
