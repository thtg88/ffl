<?php

namespace App\Repositories;

use App\Position;

class PositionRepository extends BaseRepository
{
  /**
   * Create a new repository instance.
   *
   * @param \App\Position
   * @return  void
   */
  public function __construct(Position $position)
  {
    $this->model = $position;
  }

  /**
   * Return a position from a given abbreviation.
   *
   * @param string  $abbreviation The abbreviation of the position.
   * @return  \App\Position
   */
  public function getByAbbreviation($abbreviation)
  {
    return $this->model
      ->where('abbreviation', $abbreviation)
      ->first();
  }

  /**
   * Return the count for each position id from a given season id.
   *
   * @param int $season_id  The season id.
   * @return  Collection
   */
  public function getAllCountBySeason($season_id)
  {
    return $this->model
      ->select('positions.id', \DB::raw('count(*) as total'))
      ->join('season_position', 'positions.id', '=', 'position_id')
      ->groupBy('positions.id')
      ->groupBy('season_id')
      ->where('season_id', $season_id)
      ->get();
  }

  /**
   * Return the count from a given position and season id.
   *
   * @param int $position_id  The position id.
   * @param int $season_id  The season id.
   * @return  int
   */
  public function getCountByPositionAndSeason($position_id, $season_id)
  {
    $positions = $this->getByPositionAndSeason($position_id, $season_id);
    return count($positions);
  }

  /**
   * Return the positions from a given position id and season id.
   *
   * @param int $position_id  The position id.
   * @param int $season_id  The season id.
   * @return  Collection
   */
  public function getByPositionAndSeason($position_id, $season_id)
  {
    return $this->model
      ->select('positions.*')
      ->join('season_position', 'positions.id', '=', 'position_id')
      ->where('positions.id', $position_id)
      ->where('season_id', $season_id)
      ->get();
  }

  /**
   * Return all the positions, sorted by sequence.
   *
   * @return  Collection
   */
  public function all()
  {
    return $this->model
      ->orderBy('sequence', 'asc')
      ->get();
  }
}
