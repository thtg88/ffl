<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Market extends Model
{
	protected $dates = [
		'finished_at',
		'started_at',
	];

	protected $fillable = [
		'finished_at',
		'name',
		'season_id',
		'started_at',
		'updated_at',
	];

	public function setStartedAtAttribute($value)
	{
		$this->attributes['started_at'] = empty($value) ? NULL : $value;
	}

	public function setFinishedAtAttribute($value)
	{
		$this->attributes['finished_at'] = empty($value) ? NULL : $value;
	}

	public function season()
	{
		return $this->belongsTo('App\Season');
	}

	// How many per each position should be allowed in a market for a fantateam in a season
	public function positions()
	{
		return $this->belongsToMany('App\Position', 'market_position');
	}

	public function fantateams()
	{
		return $this->belongsToMany('App\Fantateam', 'market_fantateam', 'market_id', 'fantateam_id');
	}

	public function offers()
	{
		return $this->hasMany('App\Offer');
	}
}
