<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'email',
		'is_admin',
		'name',
		'password',
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token'
	];

	public function seasons()
	{
		return $this->belongsToMany('App\Season', 'user_season');
	}

	public function fantateams()
	{
		return $this->hasMany('App\Fantateam');
	}
}
