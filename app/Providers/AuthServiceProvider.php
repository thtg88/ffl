<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Model;
use App\Policies\ModelPolicy;
use App\Position;
use App\Policies\PositionPolicy;
use App\Formation;
use App\Policies\FormationPolicy;
use App\Season;
use App\Policies\SeasonPolicy;
use App\Fantateam;
use App\Policies\FantateamPolicy;
use App\Market;
use App\Policies\MarketPolicy;
use App\Player;
use App\Policies\PlayerPolicy;
use App\Offer;
use App\Policies\OfferPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
		'App\Model' => 'App\Policies\ModelPolicy',
		Position::class => PositionPolicy::class,
		Formation::class => FormationPolicy::class,
		Season::class => SeasonPolicy::class,
		Fantateam::class => FantateamPolicy::class,
		Market::class => MarketPolicy::class,
		Player::class => PlayerPolicy::class,
		Offer::class => OfferPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
    }
}
