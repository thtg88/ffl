<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/Routes/fantateams.php');
            require app_path('Http/Routes/formations.php');
            require app_path('Http/Routes/main.php');
            require app_path('Http/Routes/markets.php');
            require app_path('Http/Routes/offers.php');
            require app_path('Http/Routes/players.php');
            require app_path('Http/Routes/positions.php');
            require app_path('Http/Routes/seasons.php');
            require app_path('Http/Routes/teams.php');
            require app_path('Http/Routes/unconfirmed_players.php');
            require app_path('Http/Routes/users.php');
        });
    }
}
