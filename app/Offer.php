<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
	protected $fillable = [
		'amount_offered',
		'fantateam_id',
		'market_id',
		'player_id',
	];

	public function market()
	{
		return $this->belongsTo('App\Market');
	}

	public function fantateam()
	{
		return $this->belongsTo('App\Fantateam');
	}

	public function player()
	{
		return $this->belongsTo('App\Player');
	}
}
