<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
	protected $dates = [
		'date_finished',
		'date_started',
	];

	protected $fillable = [
		'current_day',
		'fantateam_budget',
		'date_finished',
		'date_started',
		'is_active',
		'name',
		'number_days',
	];

	public function days()
	{
		return $this->hasMany('App\Day');
	}

	// How many per each position should be allowed in a fantateam in a season
	public function positions()
	{
		return $this->belongsToMany('App\Position', 'season_position');
	}

	public function formations() {
		return $this->belongsToMany('App\Formation', 'season_formation');
	}

	public function users()
	{
		return $this->belongsToMany('App\User', 'user_season');
	}

	public function fantateams()
	{
		return $this->hasMany('App\Fantateam');
	}

	public function players()
	{
		return $this->hasMany('App\Player');
	}

	public function markets()
	{
		return $this->hasMany('App\Market');
	}
}
