<?php

namespace App\Http\Middleware;

use Closure;

use App\Repositories\FantateamRepository;

class IsFantateamSeasonActiveMiddleware
{
  /**
   * The fantateam repository implementation.
   *
   * @var FantateamRepository
   */
  protected $fantateams;

  /**
   * Create a new middleware instance.
   *
   * @param	FantateamRepository	$fantateams
   * @return void
   */
  public function __construct(FantateamRepository $fantateams)
  {
    $this->fantateams = $fantateams;
  }

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    // We assume $request->id contains the fantateam id
    if($request->id === null || !is_numeric($request->id))
    {
      abort(404, 'Fantateam not found.');
    }

    // Get fantateam
    $fantateam = $this->fantateams->find($request->id);
    if($fantateam === null)
    {
      // 404 if not found
      abort(404, 'Fantateam not found!');
    }

    if($fantateam->season->is_active == 0)
		{
      // If season is not active go back
      return back()->with('season_not_active', 1);
    }
    
    return $next($request);
  }
}
