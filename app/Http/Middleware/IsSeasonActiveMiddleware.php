<?php

namespace App\Http\Middleware;

use Closure;

use App\Repositories\SeasonRepository;

class IsSeasonActiveMiddleware
{
  /**
   * The fantateam repository implementation.
   *
   * @var SeasonRepository
   */
  protected $seasons;

  /**
   * Create a new middleware instance.
   *
   * @param	SeasonRepository	$fantateams
   * @return void
   */
  public function __construct(SeasonRepository $seasons)
  {
    $this->seasons = $seasons;
  }

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if(isset($request->season_id))
    {
      // We assume $request->season_id contains the season id
      if(!is_numeric($request->season_id))
      {
        // 404 if not set or not numeric (to avoid DB errors)
        abort(404, 'Season not found.');
      }

      $season_id = $request->season_id;
    }
    else
    {
      // We assume $request->id contains the season id
      if($request->id === null || !is_numeric($request->id))
      {
        // 404 if not set or not numeric (to avoid DB errors)
        abort(404, 'Season not found.');
      }

      $season_id = $request->id;
    }

    // Get Season
    $season = $this->seasons->find($season_id);
    if($season === null)
    {
      // 404 if not found
      abort(404, 'Season not found!');
    }

    if($season->is_active == 0)
    {
      // If season is not active go back
      return back()->with('season_not_active', 1);
    }

    return $next($request);
  }
}
