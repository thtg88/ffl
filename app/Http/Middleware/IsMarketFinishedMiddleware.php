<?php

namespace App\Http\Middleware;

use Closure;

use App\Repositories\MarketRepository;

class IsMarketFinishedMiddleware
{
  /**
   * The market repository implementation.
   *
   * @var MarketRepository
   */
  protected $markets;

  /**
	 * Create a new controller instance.
	 *
   * @param MarketRepository $markets
   * @return void
	 */
	public function __construct(MarketRepository $markets)
  {
		$this->markets = $markets;
	}

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if(isset($request->market_id))
    {
      // In case request does not come from MarketController

      // Get market
      $market = $this->markets->find($request->market_id);
      if($market === null)
      {
        // 404 if not found
        abort(404, 'Market not found.');
      }

      if(config('app.now') >= $market->finished_at)
      {
        return back()->with('market_finished', 1);
      }
    }
    // TODO replace with $request->id?
    elseif(isset($request->market) && isset($request->market->finished_at) && config('app.now') >= $request->market->finished_at)
    {
      return back()->with('market_finished', 1);
    }
    return $next($request);
  }
}
