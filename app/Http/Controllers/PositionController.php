<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\PositionRepository;

use App\Position;

class PositionController extends Controller
{
  /**
   * The position repository implementation.
   *
   * @var PositionRepository
   */
  protected $positions;

  /**
	 * Create a new controller instance.
	 *
   * @param PositionRepository  $positions
	 * @return void
	 */
	public function __construct(PositionRepository $positions)
  {
    $this->positions = $positions;
	}

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
	public function index()
  {
    // Get
    $positions = $this->positions->all();
    
		return view('admin.positions.index.main', compact('positions'));
	}

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
	public function store(Request $request)
  {
    // Validate
		$this->validate($request, [
			'name' => 'required|max:255',
			'abbreviation' => 'required|max:5',
			'sequence' => 'required|integer',
		]);

    // Create
    $position = $this->positions->create($request->all());

    return back();
	}

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function destroy($id)
  {
    // Delete
		$this->positions->destroy($id);

    return back();
	}
}
