<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Offer;
use App\Repositories\FantateamRepository;
use App\Repositories\OfferRepository;
use App\Repositories\PlayerRepository;
use App\Repositories\PositionRepository;

class OfferController extends Controller
{
	/**
	 * The fantateam repository implementation.
	 *
	 * @var FantateamRepository
	 */
	protected $fantateams;

	/**
	 * The fantateam repository implementation.
	 *
	 * @var OfferRepository
	 */
	protected $offers;

	/**
	 * The fantateam repository implementation.
	 *
	 * @var PlayerRepository
	 */
	protected $players;

	/**
	 * The fantateam repository implementation.
	 *
	 * @var PositionRepository
	 */
	protected $positions;

	/**
	 * Create a new controller instance.
	 *
	 * @param	FantateamRepository	$fantateams
	 * @param	OfferRepository	$offers
	 * @param	PlayerRepository	$players
	 * @param	PositionRepository	$positions
	 * @return	void
	 */
	public function __construct(FantateamRepository $fantateams, OfferRepository $offers, PlayerRepository $players, PositionRepository $positions)
	{
		$this->fantateams = $fantateams;
		$this->offers = $offers;
		$this->players = $players;
		$this->positions = $positions;
	}

	/**
	 * Returns the validation rules.
	 *
	 * @param	int	$fantateam_id	The id of the fantateam.
	 * @param	int	$fantateam_budget	The budget of the fantateam.
	 * @return	array
	 */
	protected function getValidationRules($fantateam_budget)
	{
		return [
			'amount_offered' => 'required|integer|min:1',
			'fantateam_id' => 'required|exists:fantateams,id',
			'market_id' => 'required|exists:markets,id',
			'player_id' => 'required|exists:players,id',
		];
	}

	/**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
	public function store(Request $request)
  {
    // Get fantateam
    $fantateam = $this->fantateams->find($request->fantateam_id);
		if($fantateam === null)
		{
			abort(404, 'Fantateam not found.');
		}

		// Authorize
		$this->authorize('addOffer', $fantateam);

		// Validate
		$this->validate($request, $this->getValidationRules($fantateam->budget));

		// Get Market
		$market = $this->players->find($request->market_id);
		if($market === null)
		{
			abort(404, 'Market not found.');
		}

		// Get Player
		$player = $this->players->find($request->player_id);
		if($player === null)
		{
			abort(404, 'Player not found.');
		}

    // Get market Max Offers
		$max_offers = $this->offers->getMarketMaxOffers($market->id);

    // Get offers
		$offers = $this->offers->getFantateamOffers($fantateam->id, $market->id);

		$players_winning = count($fantateam->players);
		foreach($offers as $offer)
    {
			foreach($max_offers as $max_offer)
      {
				if($max_offer->player_id == $offer->player_id)
        {
					if($max_offer->amount_offered > $offer->amount_offered)
          {
						$offer->is_max = 0;
					}
          else
          {
						$offer->is_max = 1;
						$fantateam->budget -= $offer->amount_offered;
						$players_winning++;
					}
					break;
				}
			}
		}

    // The maximum I can spend for a player is my budget
    // minus the remaining players I have to buy
		// (assuming the minimum cost of a player is 1)
		$max_spend_per_player = $fantateam->budget - count($market->season->positions) + $players_winning;

    // Get number of player per position allowed this season
		$max_positions = $this->positions->getAllCountBySeason($market->season->id);

    // Get the count of players for each positions in my current team
    $position_fantateam = $this->players->getFantateamPlayersPositionCount($fantateam->id);

    // Get current offer
		$current_offer = $this->offers->getByMarketFantateamAndPlayer($market->id, $fantateam->id, $player->id);
		if($current_offer === NULL)
    {
			// Validate
      $this->validate($request, [
				'amount_offered' => 'required|integer|min:'.$player->cost.'|max:'.$max_spend_per_player,
			]);

			// This Calculates if I am still allowed to offer for a certain position
			$rem_positions = array();
			foreach($max_positions as $position)
	    {
				$rem_positions[$position->id] = $position->total;

	      // First check my current fantateam
				foreach($position_fantateam as $fantateam_position)
	      {
					if($fantateam_position == $position->id)
	        {
						$rem_positions[$position->id] -= $fantateam_position->total;
						if($rem_positions[$position->id] == 0)
	          {
							// TODO flash Session variable
	            return back();
	          }
						break;
					}
				}

	      // Then check Current Offers
				foreach($offers as $offer)
	      {
					if($position->id == $offer->player->position_id && $offer->is_max)
	        {
						if($rem_positions[$position->id] > 1)
	          {
	            $rem_positions[$position->id]--;
	          }
						else if($player->position_id == $position->id)
	          {
							// TODO flash Session variable
	            return back();
	          }
					}
				}
			}

			// If not found create
      $this->offers->create($request->all());
		}
    else
    {
			// Validate
      $this->validate($request, [
				'amount_offered' => 'required|integer|min:'.max($current_offer->amount_offered, $player->cost).'|max:'.$max_spend_per_player,
			]);

			// Update
      $this->offers->update($current_offer->id, [
				'amount_offered' => $request->amount_offered
			]);
		}

		return back();
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param	\App\Offer	$offer
	 * @param	\Illuminate\Http\Request	$request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Offer $offer, Request $request)
	{
		// Authorize
		$this->authorize('edit', $offer);

		if($offer->market->finished_at > config('app.now'))
		{
			// Get fantateam
			$fantateam = $this->fantateams->find($request->fantateam_id);

			// Validate
			$this->validate($request, $this->getValidationRules($request->fantateam_id, $fantateam->budget));

			// Update
			$this->offers->update($offer->id, $request->all());
		}

		return back();
	}

	/**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function destroy(Offer $offer)
	{
		// Authorize
		$this->authorize('destroy', $offer);

		if(config('app.now') < $offer->market->finished_at)
		{
			// If market not finished - Delete
			$this->offers->destroy($offer->id);
		}
		return back();
	}
}
