<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Repositories\FormationRepository;
use App\Repositories\PositionRepository;

class FormationController extends Controller
{
  /**
   * The formation repository implementation.
   *
   * @var FormationRepository
   */
  protected $formations;

  /**
   * The position repository implementation.
   *
   * @var PositionRepository
   */
  protected $positions;

  /**
   * The controller validation rules.
   *
   * @var array
   */
  private $validation_rules;

  /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(FormationRepository $formations, PositionRepository $positions)
  {
		$this->formations = $formations;
    $this->positions = $positions;

    $this->validation_rules = [
			'name' => 'required|max:10',
			'positions' => 'required|array',
			'positions.*.count' => 'required|numeric|min:1',
		];
	}

  /**
   * Returns the validation rules for this controller.
   *
   * @return array
   */
  protected function getValidationRules()
  {
    return $this->validation_rules;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
	public function index()
  {
    // Get formations
    $formations = $this->formations->all();

    // Get positions
		$positions = $this->positions->all();

    return view('admin.formations.index.main', compact('formations','positions'));
	}

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
	public function store(Request $request)
  {
    // Validate
		$this->validate($request, $this->getValidationRules());

    // Create
    $formation = $this->formations->create($request->all());

    return back();
	}

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function destroy($id)
  {
    // Delete
    $response = $this->formations->destroy($id);

    return back();
	}
}
