<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Goutte\Client;
use League\Csv\Reader;

use App\Repositories\PlayerRepository;
use App\Repositories\PositionRepository;
use App\Repositories\SeasonRepository;
use App\Repositories\TeamRepository;
use App\Repositories\UnconfirmedPlayerRepository;

class UnconfirmedPlayerController extends Controller
{
  /**
   * The player repository implementation.
   *
   * @var PlayerRepository
   */
  protected $players;

  /**
   * The player repository implementation.
   *
   * @var PositionRepository
   */
  protected $positions;

  /**
   * The player repository implementation.
   *
   * @var SeasonRepository
   */
  protected $seasons;

  /**
   * The team repository implementation.
   *
   * @var TeamRepository
   */
  protected $teams;

  /**
   * The unconfirmed player repository implementation.
   *
   * @var UnconfirmedPlayerRepository
   */
  protected $unconfirmed_players;

  /**
	 * Create a new controller instance.
	 *
   * @param PlayerRepository  $players
   * @param PositionRepository  $positions
   * @param SeasonRepository  $seasons
   * @param TeamRepository  $teams
   * @param UnconfirmedPlayerRepository  $unconfirmed_players
	 * @return void
	 */
	public function __construct(PlayerRepository $players, PositionRepository $positions, SeasonRepository $seasons, TeamRepository $teams, UnconfirmedPlayerRepository $unconfirmed_players)
  {
    $this->players = $players;
    $this->positions = $positions;
		$this->seasons = $seasons;
    $this->teams = $teams;
    $this->unconfirmed_players = $unconfirmed_players;
	}

  /**
   * Fetch a list of the specified resource from the gazzetta.it website and
   * store in storage.
   *
   * @param \Illuminate\Http\Request
   * @return \Illuminate\Http\Response
   */
  public function fetch(Request $request)
  {
    $this->validate($request, [
			'season_id' => 'required|integer|exists:seasons,id',
		]);

    // Get season
    $season = $this->seasons->find($request->season_id);

    try {
      // Create Goutte client
      $client = new Client();

      // Go to the gazzetta.it page holding the palyers' stats
      $crawler = $client->request('GET', 'http://www.gazzetta.it/calcio/fantanews/statistiche/serie-a-2016-17/');

      // Crawl through all the table's rows
      $crawler->filter('table.playerStats > tbody > tr')->each(function ($node) use($season) {
        $input = [];
        // Get team name
        $team_name = ucwords($node->filter('td.field-sqd > span.hidden-team-name')->first()->text());
        // Get team
  			$team = $this->teams->getByName($team_name);
  			if($team === null)
        {
          // Create team if not found
          $team = $this->teams->create(['name' => $team_name]);
  			}

        $position_abbreviation = trim($node->filter('td.field-ruolo')->first()->text());
        if($position_abbreviation === 'T (A)')
        {
          $position_abbreviation = 'A';
        }
        else if($position_abbreviation === 'T (C)')
        {
          $position_abbreviation = 'A';
        }
        // Get position
  			$position = $this->positions->getByAbbreviation($position_abbreviation);
  			if($position === null)
        {
          // Skip iteration if position not found
          // TODO error somehow?
          return false;
        }

        // Get player name
        $name = trim($node->filter('td.field-giocatore > a')->first()->text());
        // Get player
  			$player = $this->players->getByNamePositionAndSeason($season->id, $name, $position->id);
  			if($player === null)
        {
          // If not found - get unconfirmed player
  				$player = $this->unconfirmed_players->getByNamePositionAndSeason($season->id, $position_abbreviation, $position->id);
  			}
  			if($player === null)
        {
          // If not found - assign season id
          $input['season_id'] = $season->id;
  			}

        $input['team_id'] = $team->id;
        $input['name'] = $name;
        $input['position_id'] = $position->id;
        $input['cost'] = abs(trim($node->filter('td.field-q')->first()->text()));
        $input['games_played'] = abs(trim($node->filter('td.field-pg')->first()->text()));
        $input['goals_scored_conceived'] = abs(trim($node->filter('td.field-g')->first()->text()));
        $input['assists'] = abs(trim($node->filter('td.field-a')->first()->text()));
        $input['yellow_cards'] = abs(trim($node->filter('td.field-am')->first()->text()));
        $input['red_cards'] = abs(trim($node->filter('td.field-es')->first()->text()));
        $input['penalties_shot'] = abs(trim($node->filter('td.field-rt')->first()->text()));
        $input['penalties_scored_saved'] = abs(trim($node->filter('td.field-r')->first()->text()));
        $input['penalties_missed'] = abs(trim($node->filter('td.field-rs')->first()->text()));
        $input['penalties_saved'] = abs(trim($node->filter('td.field-rp')->first()->text()));
        $input['mark_avg_percent'] = abs(trim($node->filter('td.field-mv')->first()->text()));
        $input['magic_avg_percent'] = abs(trim($node->filter('td.field-mm')->first()->text()));
        $input['magic_points_percent'] = abs(trim($node->filter('td.field-mp')->first()->text()));

        if($player === null)
        {
          // If not found - create unconfirmed player
          $this->unconfirmed_players->create($input);
  			}
        else
        {
          if($player instanceof \App\UnconfirmedPlayer)
          {
            // Update unconfirmed player
            $this->players->update($player->id, $input);
          }
          elseif($player instanceof \App\Player)
          {
            // Update player
            $this->players->update($player->id, $input);
          }
        }
      });
    } catch (Exception $e) {
      return back()->with('fetch_error', $e->getMessage());
    }
    return back();
  }

  /**
   * Upload a file containing a list of the specified resource and store in
   * storage.
   *
   * @param \Illuminate\Http\Request
   * @return  \Illuminate\Http\Response
   */
  public function upload(Request $request)
  {
	  $this->validate($request, [
			'file' => 'required|file',
			'csv_format' => 'required|in:uk,eu',
      'contains_header' => 'required|boolean',
			'season_id' => 'required|integer|exists:seasons,id',
		]);

    $unconfirmed_players_file = $request->file('file');
		if($unconfirmed_players_file->isValid() === FALSE || strtolower($unconfirmed_players_file->getClientOriginalExtension()) !== "csv")
    {
      // TODO flash session variable
      return back();
    }

    // Get season
    $season = $this->seasons->find($request->season_id);

  	$csv = Reader::createFromPath($unconfirmed_players_file->getPathName());
		$csv_format = $request->csv_format;
		$contains_header = $request->contains_header;
		if($csv_format === "eu")
    {
			$csv->setDelimiter(';');
			$decimal_separator = ",";
			$thousand_separator = ".";
		}
    else
    {
			$csv->setDelimiter(',');
			$decimal_separator = ".";
			$thousand_separator = ",";
		}
		$csv->setNewline("\r\n");

		foreach($csv as $index => $row)
    {
      // Skip iteration if first row and file has header row, or row has less than 2 columns
			if(($contains_header != 0 && $index == 0) || count($row) < 2)
      {
        continue;
      }

      // Get team
			$team = $this->teams->getByName($row[0]);
			if($team === null)
      {
        // Create team if not found
        $team = $this->teams->create(['name' => $row[0]]);
			}

      // Get position
			$position = $this->positions->getByAbbreviation($row[2]);
			if($position === null)
      {
        // Skip iteration if position not found
        // TODO error somehow?
        continue;
      }

      // Get player
			$player = $this->players->getByNamePositionAndSeason($season->id, $row[1], $position->id);
			if($player === null)
      {
        // If not found - get unconfirmed player
				$player = $this->unconfirmed_players->getByNamePositionAndSeason($season->id, $row[1], $position->id);
			}
			if($player === null)
      {
        // If not found - assign season id
        $input['season_id'] = $season->id;
			}

			foreach($row as $idx => $value)
      {
				switch($idx)
        {
					case 0:
						$input['team_id'] = $team->id;
						break;
					case 1:
						$input['name'] = $value;
						break;
					case 2:
						$input['position_id'] = $position->id;
						break;
					case 3:
						$input['cost'] = abs($value);
						break;
					case 4:
						$input['games_played'] = abs($value);
						break;
					case 5:
						$input['goals_scored_conceived'] = $value;
						break;
					case 6:
						$input['assists'] = abs($value);
						break;
					case 7:
						$input['yellow_cards'] = abs($value);
						break;
					case 8:
						$input['red_cards'] = abs($value);
						break;
					case 9:
						$input['penalties_shot'] = abs($value);
						break;
					case 10:
						$input['penalties_scored_saved'] = abs($value);
						break;
					case 11:
						$input['penalties_missed'] = abs($value);
						break;
					case 12:
						$input['penalties_saved'] = abs($value);
						break;
					case 13:
						$input['mark_avg_percent'] = str_replace($thousand_separator, "", str_replace($decimal_separator, "", $value * 100));
						break;
					case 14:
						$input['magic_avg_percent'] = str_replace($thousand_separator, "", str_replace($decimal_separator, "", $value * 100));
						break;
					case 15:
						$input['magic_points_percent'] = str_replace($thousand_separator, "", str_replace($decimal_separator, "", $value));
						break;
				}
			}

      if($player === null)
      {
        // If not found - create unconfirmed player
        $this->unconfirmed_players->create($input);
			}
      else
      {
        if($player instanceof \App\UnconfirmedPlayer)
        {
          // Update unconfirmed player
          $this->players->update($player->id, $input);
        }
        elseif($player instanceof \App\Player)
        {
          // Update player
          $this->players->update($player->id, $input);
        }
      }
		}
		return back();
	}

  /**
   * Confirm the specified resource.
   *
   * @param int $id The unconfirmed player id.
   * @return  \Illuminate\Http\Response
   */
	public function confirm($id)
  {
    // Confirm
    $response = $this->unconfirmed_players->confirm($id);
    if($response === null)
    {
      abort(404, 'Unconfirmed player not found.');
    }

		return back();
	}

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function destroy($id)
  {
    // Delete
    $this->unconfirmed_players->destroy($id);

		return back();
	}
}
