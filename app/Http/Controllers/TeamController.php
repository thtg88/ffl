<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;

use App\Repositories\PlayerRepository;
use App\Repositories\TeamRepository;

class TeamController extends Controller
{
	/**
	 * The team repository implementation.
	 *
	 * @var	TeamRepository
	 */
	protected $teams;

	/**
	 * The player repository.
	 *
	 * @var	PlayerRepository
	 */
	protected $players;

	/**
	 * Create a new controller instance.
	 *
	 * @param	TeamRepository	$teams
	 * @param	PlayerRepository	$players
	 * @return void
	 */
	public function __construct(TeamRepository $teams, PlayerRepository $players)
	{
		$this->players = $players;
		$this->teams = $teams;
	}

	/**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
	public function index()
	{
		// Get
		$teams = $this->teams->all();

		return view('admin.teams.index.main', compact('teams'));
	}

	/**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function show($id)
	{
		// Get
		$team = $this->teams->find($id);
		if($team === null)
		{
			abort(404, 'Team not found.');
		}

		// Get players
		$players = $this->players->getTeamPlayers($id);

		return view('admin.teams.single.main', compact('team','players'));
	}

	/**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
	public function store(Request $request)
	{
		// Validate
		$this->validate($request, [
			'name' => 'required|max:255',
			'logo' => 'required', //|mimes:image/jpeg,image/png
		]);

		if($request->file('logo')->isValid())
		{
			// Create Team
			$team = $this->teams->create(['name' => $request->name]);

			$logo = $request->file('logo');

			$filename = $team->id.'.'.$logo->getClientOriginalExtension();
			$logo_path = 'img/teams/logos/';

			// Move Image to FS
			$logo->move($logo_path, $filename);

			// Resize Image to 50 width (maintaining aspect Ratio)
			$img = Image::make($logo_path.$filename);
			$img->resize(null, 50, function ($constraint) {
				$constraint->aspectRatio();
			});
			$img->save();
			$logo_path = '/'.$logo_path.$filename;

			// Update Logo Path
			$team->logo_path = $logo_path;
			$team->save();
		}

		return back();
	}

	/**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function destroy($id)
	{
		// Delete
		$this->teams->destroy($id);
		
		return back();
	}
}
