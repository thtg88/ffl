<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

use App\Fantateam;
use App\Market;
use App\Player;
use App\Repositories\FantateamRepository;
use App\Repositories\MarketRepository;
use App\Repositories\OfferRepository;
use App\Repositories\PlayerRepository;
use App\Repositories\PositionRepository;
use App\Repositories\SeasonRepository;

class MarketController extends Controller
{
  /**
   * The fantateam repository implementation.
   *
   * @var FantateamRepository
   */
  protected $fantateams;

  /**
   * The market repository implementation.
   *
   * @var MarketRepository
   */
  protected $markets;

  /**
   * The offers repository implementation.
   *
   * @var OfferRepository
   */
  protected $offers;

  /**
   * The player repository implementation.
   *
   * @var PlayerRepository
   */
  protected $players;

  /**
   * The position repository implementation.
   *
   * @var PositionRepository
   */
  protected $positions;

  /**
   * The season repository implementation.
   *
   * @var SeasonRepository
   */
  protected $seasons;

  /**
	 * Create a new controller instance.
	 *
   * @param FantateamRepository $fantateams
   * @param MarketRepository $markets
   * @param OfferRepository $offers
   * @param PlayerRepository $players
   * @param PositionRepository $positions
   * @param SeasonRepository $seasons
   * @return void
	 */
	public function __construct(FantateamRepository $fantateams, MarketRepository $markets, OfferRepository $offers, PlayerRepository $players, PositionRepository $positions, SeasonRepository $seasons)
  {
		$this->fantateams = $fantateams;
    $this->markets = $markets;
    $this->offers = $offers;
    $this->players = $players;
    $this->positions = $positions;
    $this->seasons = $seasons;
	}

  /**
   * Returns the validation rules for this controller.
   *
   * @return  array
   */
  protected function getValidationRules()
  {
    // TODO check dates not overlapping with other markets in the same season
    return [
      'finished_at' => 'present|date',
      'started_at' => 'present|date',
    ];
  }

  /**
   * Returns the validation rules exclusive for the store method
   * for this controller.
   *
   * @param int $season_id  The id of the season.
   * @return  array
   */
  protected function getStoreValidationRules($season_id)
  {
    return [
      'fantateams' => 'required|array',
      'fantateams.*' => 'required|integer|exists:fantateams,id',
      'name' => 'required|max:255|unique:markets,name,NULL,id,season_id,'.$season_id,
      'season_id' => 'required|integer|exists:seasons,id',
    ];
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id The id of the market.
   * @return \Illuminate\Http\Response
   */
	public function show($id)
  {
    // Get market
    $market = $this->markets->find($id);
    if($market === null)
    {
      abort(404, 'Market not found.');
    }

    $season = $market->season;
    $season_fantateams = $season->fantateams;
		$market_fantateams = $market->fantateams;

    $rem_fantateams = array();
		foreach($season_fantateams as $fantateam)
    {
      if(!$market_fantateams->contains($fantateam->id))
      {
        $rem_fantateams[] = $fantateam;
      }
    }

    $offers = $market->offers;

    return view('admin.markets.show.main', compact('season', 'market', 'market_fantateams', 'rem_fantateams', 'offers'));
	}

  /**
   * Display the offers associated to the specified resource.
   *
   * @param int $id The id of the market.
   * @param int $player_id  The id of the player
   * @return  \Illuminate\Http\Response
   */
	public function showPlayerOffers($id, $player_id)
  {
    // Get market
    $market = $this->markets->find($id);
    if($market === null)
    {
      abort(404, 'Market not found.');
    }

    // Get player
    $player = $this->players->find($player_id);
    if($player === null)
    {
      abort(404, 'Player not found.');
    }

		$season = $market->season;
		$offers = $this->offers->getPlayerOffers($market->id,$player->id);
		$fantateam = $this->fantateams->getUserFantateam(\Auth::user()->id, $market->season_id);

    // All Other Fantateam Offers
		$all_other_offers = $this->offers->getOtherFantateamsOffers($market->id,$fantateam->id);

    // Offers
		$my_offers = $this->offers->getFantateamOffers($market->id, $fantateam->id);

		$players_winning = count($fantateam->players);
		foreach($my_offers as $offer)
    {
			$player_matched = FALSE;
			foreach($all_other_offers as $other_offer)
      {
				if($other_offer->player_id == $offer->player_id)
        {
					$player_matched = TRUE;
					// If greater offer from someone else or equals but done earlier -> not max
					if($other_offer->amount_offered > $offer->amount_offered || $other_offer->amount_offered == $offer->amount_offered && $other_offer->id < $offer->id)
          {
						$offer->is_max = 0;
					}
          else
          {
						$offer->is_max = 1;
						$fantateam->budget -= $offer->amount_offered;
						$players_winning++;
					}
				}
			}
			if($player_matched === FALSE)
      {
				$offer->is_max = 1;
				$fantateam->budget -= $offer->amount_offered;
				$players_winning++;
			}
		}
		// The maximum I can spend for a player is my budget
    // minus the remaining players I have to buy
		// (considered the minimum cost of a plyer is 1)
		$max_spend_per_player = $fantateam->budget - count($season->positions) + $players_winning;

    return view('markets.player-offers.main', compact('fantateam','season','market','player','offers','max_spend_per_player'));
	}

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
	public function store(Request $request)
  {
    // Validate
    $this->validate($request, array_merge($this->getValidationRules(), $this->getStoreValidationRules($request->season_id)));

    // Get season
    $season = $this->seasons->find($request->season_id);
    if($season === null)
    {
      abort(404, 'Season not found.');
    }

    if($season->is_active)
    {
			// TODO swap dates if inverted
			/*
			if($request->started_at > $request->finished_at)
      {
				$temp = $request->started_at;
				$request->started_at = $request->finished_at;
				$request->finished_at = $temp;
			}
			*/

      // Create
			$market = $this->markets->create($request->all());
		}
    else
    {
      // TODO flash session variable
    }

		return back();
	}

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id The id of the market.
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
	public function update($id, Request $request)
  {
    // Get market
    $market = $this->markets->find($id);
    if($market === null)
    {
      abort(404, 'Market not found.');
    }

    if($market->season->is_active)
    {
      // Validate
			$this->validate($request, $this->getValidationRules());

      // Update
      $this->markets->update($market->id, $request->all());
		}

		return back();
	}

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id The id of the market.
   * @return \Illuminate\Http\Response
   */
	public function destroy($id)
  {
    // Delete
		$this->markets->destroy($id);

    return back();
	}

  /**
   * Attach a fantateam to the specified resource.
   *
   * @param int The id of the market.
   * @param \Illuminate\Http\Request
   * @return  \Illuminate\Http\Response
   */
	public function addFantateam($id, Request $request)
  {
    // Get market
    $market = $this->markets->find($id);
    if($market === null)
    {
      abort(404, 'Market not found.');
    }

    // Validate
		$this->validate($request, [
			'fantateam_id' => 'required|exists:fantateams,id|unique:market_fantateam,fantateam_id,NULL,id,market_id,' . $market->id,
		]);

		if(!$market->fantateams->contains($request->fantateam_id))
    {
      // Add Fantateam only if fantateam not already available
      $market->fantateams()->attach($request->fantateam_id);
    }

		return back();
	}

  /**
   * Detach a fantateam from the specified resource.
   *
   * @param int The id of the market.
   * @param int The id of the fantateam.
   * @return  \Illuminate\Http\Response
   */
	public function removeFantateam($id, $fantateam_id)
  {
    // Get market
    $market = $this->markets->find($id);
    if($market === null)
    {
      abort(404, 'Market not found.');
    }

    // Get fantateam
    $fantateam = $this->fantateams->find($id);
    if($fantateam === null)
    {
      abort(404, 'Fantateam not found.');
    }

    // Detach
    $market->fantateams()->detach($fantateam->id);

    return back();
	}

  /**
   * Close the specified resource, assigning the won players to the highest
   * offering fantateam.
   *
   * @param int $id The id of the market.
   * @return  \Illuminate\Http\Response
   */
  public function close($id)
  {
    // Get market
    $market = $this->markets->find($id);
    if($market === null)
    {
      abort(404, 'Market not found.');
    }

    // Mark Finished Date so people can't offer anymore
    $market = $this->markets->update($market->id, [
      'finished_at' => config('app.now')->toDateTimeString(),
    ]);

    // Get season
    $season = $market->season;

    // Get market fantateams
    $fantateams = $market->fantateams;

    // Assoc-array: fantateam id as key, budget as value
    $fantateam_budgets = [];

    // Reset all fantateams budget back to season's
    foreach ($fantateams as $idx => $temp_fantateam)
    {
      $fantateam_budgets[$temp_fantateam->id] = $season->fantateam_budget;
    }

    // Get all market offers
    $all_offers = $market->offers;

    // Get market Max Offers
		$max_offers = $this->offers->getMarketMaxOffers($market->id);

    // Mark all the players to the highest offering fantateam
    foreach($all_offers as $offer)
    {
      foreach($max_offers as $max_offer)
      {
        if($max_offer->player_id == $offer->player_id)
        {
          // If maximum offer and player not already assigned
          if($offer->amount_offered >= $max_offer->amount && $offer->player->fantateam_id === null)
          {
            $this->players->update($offer->player_id, [
              'fantateam_id' => $offer->fantateam_id,
            ]);

            // Update temp budget
            $fantateam_budgets[$offer->fantateam_id] -= $offer->amount_offered;
          }
          break;
        }
      }
    }

    // Update all fantateams budget
    foreach($fantateams as $temp_fantateam)
    {
      $this->fantateams->update($temp_fantateam->id, [
        'budget' => $fantateam_budgets[$temp_fantateam->id],
      ]);
		}

    // Get number of player per position allowed this season
		$max_positions = $this->positions->getAllCountBySeason($season->id);

    // Check if fantateams are left with certain players to buy
    $new_market_fantateam_ids = [];
    foreach($fantateams as $idx => $temp_fantateam)
    {
      // Get the count of players for each positions in my fantateam
      $count_fantateam_positions = $this->players->getFantateamPlayersPositionCount($temp_fantateam->id);

      foreach($max_positions as $idx => $max_position)
      {
        $does_fantateam_need_new_market = false;
        foreach ($count_fantateam_positions as $pos_idx => $count_fantateam_position)
        {
          // If position coincides and count of my players is lower than max
          if($count_fantateam_position->position_id == $max_position->id && $count_fantateam_position->pos_count < $max_position->total)
          {
            $does_fantateam_need_new_market = true;
            break;
          }
        }
        if($does_fantateam_need_new_market === true)
        {
          $new_market_fantateam_ids[] = $temp_fantateam->id;
          break;
        }
      }
    }

    // If fantateams are left with certain players to buy, create a new market for them
    if(count($new_market_fantateam_ids) > 0)
    {
      $tomorrow = config('app.now')->copy()->addDay();
      $new_market_data = [];
      $new_market_data['fantateams'] = $new_market_fantateam_ids;
      $new_market_data['finished_at'] = $tomorrow->toDateTimeString();
      // TODO generate better name
      $new_market_data['name'] = 'blah.';
      $new_market_data['season_id'] = $season->id;
      $new_market_data['started_at'] = config('app.now')->toDateTimeString();

      // Create new market
      $market = $this->markets->create($new_market_data);

      return redirect('admin/markets/'.$market->id)->with('market_created_from_close', 1);
    }

    return back()->with('market_close_success', 1);
  }
}
