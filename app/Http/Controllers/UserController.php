<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class UserController extends Controller
{
  /**
   * The user repository implementation.
   *
   * @var UserRepository
   */
  protected $users;

  /**
	 * Create a new controller instance.
	 *
   * @param UserRepository  $users
	 * @return void
	 */
	public function __construct(UserRepository $users)
  {
    $this->users = $users;
	}

  /**
   * Display a listing of the resource.
   *
   * @return  \Illuminate\Http\Response
   */
	public function index()
  {
    // Get
		$users = $this->users->all();

		return view('admin.users.index.main', compact('users'));
	}

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request
   * @return  \Illuminate\Http\Response
   */
	public function store(Request $request)
  {
    // Validate
		$this->validate($request, [
			'name' => 'required|max:255|unique:users',
			'email' => 'required|max:255|email|confirmed|unique:users',
			'password' => 'required|min:6|max:255|confirmed',
			'is_admin' => 'required|boolean',
		]);

    // Create
		$user = $this->users->create($request->all());

		return back();
	}

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id The user id.
   * @return  \Illuminate\Http\Response
   */
	public function destroy($id)
  {
    // Delete
    $this->users->destroy($id);
    
		return back();
	}
}
