<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Repositories\FantateamRepository;
use App\Repositories\MarketRepository;
use App\Repositories\OfferRepository;
use App\Repositories\PlayerRepository;
use App\Repositories\PositionRepository;
use App\Repositories\SeasonRepository;

class HomeController extends Controller
{
	/**
	 * The fantateam repository implementation.
	 *
	 * @var FantateamRepository;
	 */
	protected $fantateams;

	/**
	 * The market repository implementation.
	 *
	 * @var MarketRepository;
	 */
	protected $markets;

	/**
	 * The offer repository implementation.
	 *
	 * @var OfferRepository;
	 */
	protected $offers;

	/**
	 * The player repository implementation.
	 *
	 * @var PlayerRepository;
	 */
	protected $players;

	/**
	 * The position repository implementation.
	 *
	 * @var PositionRepository;
	 */
	protected $positions;

	/**
	 * The season repository implementation.
	 *
	 * @var SeasonRepository;
	 */
	protected $seasons;

	/**
	 * Create a new controller instance.
	 *
	 * @param	FantateamRepository	$fantateams
	 * @param	MarketRepository	$markets
	 * @param	OfferRepository	$offers
	 * @param	PlayerRepository	$players
	 * @param	PositionRepository	$offers
	 * @param	SeasonRepository	$seasons
	 * @return void
	 */
	public function __construct(FantateamRepository $fantateams, MarketRepository $markets, OfferRepository $offers, PositionRepository $positions, PlayerRepository $players, SeasonRepository $seasons)
	{
		$this->fantateams = $fantateams;
		$this->markets = $markets;
		$this->offers = $offers;
		$this->players = $players;
		$this->positions = $positions;
		$this->seasons = $seasons;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$now = config('app.now');
		$user = \Auth::user();
		$fantateams = $user->fantateams();
		$active_season = $this->seasons->getUserActiveSeason($user->id);
		$ended_seasons = $this->seasons->getUserEndedSeasons($user->id);

		// First Next Market that the User has been registered to
		$first_next_market = NULL;
		$prev_markets = NULL;
		$all_players = NULL;
		$avail_players = NULL;
		$fantateam = NULL;
		$formations = NULL;
		$offers = NULL;
		$my_offers = NULL;
		$max_offers = NULL;
		$max_spend_per_player = NULL;
		if($active_season !== null)
		{
			$all_players = $active_season->players;
			$formations = $active_season->formations;

			// Get user fantateam
			$fantateam = $this->fantateams->getUserFantateam($user->id, $active_season->id);
			if($fantateam !== NULL)
			{
				// First Next Market that the User has been registered to (could be current)
				$first_next_market = $this->markets->getNext($fantateam->id);
				if($first_next_market !== NULL)
				{
					// Get all Other Fantateam Offers
					$all_other_offers = $this->offers->getOtherFantateamsOffers($first_next_market->id, $fantateam->id);

					// Get My Offers
					$my_offers = $this->offers->getFantateamOffers($first_next_market->id, $fantateam->id);

					$players_winning = count($fantateam->players);
					foreach($my_offers as $offer)
					{
						$player_matched = FALSE;
						foreach($all_other_offers as $other_offer)
						{
							if($other_offer->player_id == $offer->player_id)
							{
								$player_matched = TRUE;
								// If greater offer from someone else or equals but done earlier -> not max
								if($other_offer->amount_offered > $offer->amount_offered || $other_offer->amount_offered == $offer->amount_offered && $other_offer->id < $offer->id)
								{
									$offer->is_max = 0;
								}
								else
								{
									$offer->is_max = 1;
									$fantateam->budget -= $offer->amount_offered;
									$players_winning++;
								}
							}
						}
						if($player_matched === FALSE)
						{
							$offer->is_max = 1;
							$fantateam->budget -= $offer->amount_offered;
							$players_winning++;
						}
					}
					// The maximum I can spend for a player is my budget - the remaining player I have to buy
					// (considered the minimum cost of a plyer is 1)
					$max_spend_per_player = $fantateam->budget - count($active_season->positions) + $players_winning;

					$my_players = $fantateam->players;

					$position_ids_to_remove = array();
					// Get position count for the current season
					$active_season_positions_count = $this->positions->getAllCountBySeason($active_season->id);
					foreach($active_season_positions_count as $position_count)
					{
						$position_counter = 0;
						foreach($my_players as $my_player)
						{
							if($my_player->position_id == $position_count->id)
							{
								$position_counter++;
							}
						}
						foreach($my_offers as $offer)
						{
							if($offer->player->position_id == $position_count->id && $offer->is_max == 1)
							{
								$position_counter++;
							}
						}
						// Get max players per position
						$max_per_position = $this->positions->getCountByPositionAndSeason($position_count->id, $active_season->id);
						if($max_per_position <= $position_counter)
						{
							$position_ids_to_remove[] = $position_count->id;
						}
					}

					// Players I've already offered for
					$exclude_player_ids = $my_offers->pluck('player_id')->toArray();
					// Merge with player ids in my team
					$exclude_player_ids = array_merge($exclude_player_ids, $fantateam->players()->pluck('id')->toArray());

					// Get available players
					$avail_players = $this->players->getAvailablePlayers($active_season->id, $exclude_player_ids, $position_ids_to_remove);
				}

				// Get previous markets
				$prev_markets = $this->markets->getFantateamFinishedMarkets($fantateam->id);
			}
		}

		// Get all positions
		$positions = $this->positions->all();

		return view('home.main', compact(
			'active_season',
			'all_players',
			'avail_players',
			'ended_seasons',
			'fantateam',
			'first_next_market',
			'formations',
			'position_ids_to_remove',
			'positions',
			'user',
			'max_offers',
			'max_spend_per_player',
			'my_offers'
		));
	}
}
