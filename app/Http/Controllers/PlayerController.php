<?php

namespace App\Http\Controllers;

use App\Repositories\PlayerRepository;

class PlayerController extends Controller
{
	/**
	 * The player repository implementation.
	 *
	 * @var PlayerRepository
	 */
	protected $players;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(PlayerRepository $players)
	{
		$this->players = $players;
	}

	/**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function destroy($id)
	{
		// Delete
		$this->players->destroy($id);
		return back();
	}
}
