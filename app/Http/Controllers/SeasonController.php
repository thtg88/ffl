<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Validator;

use App\Http\Controllers\Controller;
use App\Repositories\FantateamRepository;
use App\Repositories\FormationRepository;
use App\Repositories\MarketRepository;
use App\Repositories\PlayerRepository;
use App\Repositories\PositionRepository;
use App\Repositories\SeasonRepository;
use App\Repositories\UnconfirmedPlayerRepository;
use App\Repositories\UserRepository;

use App\Market;
use App\Season;

class SeasonController extends Controller
{
  /**
   * The fantateam repository implementation.
   *
   * @var FantateamRepository
   */
  protected $fantateams;

  /**
   * The formation repository implementation.
   *
   * @var FormationRepository
   */
  protected $formations;

  /**
   * The market repository implementation.
   *
   * @var MarketRepository
   */
  protected $markets;

  /**
   * The player repository implementation.
   *
   * @var PlayerRepository
   */
  protected $players;

  /**
   * The position repository implementation.
   *
   * @var PositionRepository
   */
  protected $positions;

  /**
   * The season repository implementation.
   *
   * @var SeasonRepository
   */
  protected $seasons;

  /**
   * The unconfirmed player repository implementation.
   *
   * @var UnconfirmedPlayerRepository
   */
  protected $unconfirmed_players;

  /**
   * The user repository implementation.
   *
   * @var UserRepository
   */
  protected $users;

  /**
	 * Create a new controller instance.
	 *
   * @param FantateamRepository $fantateams
   * @param FormationRepository $formations
   * @param MarketRepository $markets
   * @param PlayerRepository $players
   * @param PositionRepository $positions
   * @param SeasonRepository $seasons
   * @param UnconfirmedPlayerRepository $unconfirmed_players
   * @param UserRepository $users
	 * @return void
	 */
	public function __construct(FantateamRepository $fantateams, FormationRepository $formations, MarketRepository $markets, PlayerRepository $players, PositionRepository $positions, SeasonRepository $seasons, UnconfirmedPlayerRepository $unconfirmed_players, UserRepository $users)
  {
		$this->fantateams = $fantateams;
    $this->formations = $formations;
    $this->markets = $markets;
    $this->players = $players;
    $this->positions = $positions;
    $this->seasons = $seasons;
    $this->unconfirmed_players = $unconfirmed_players;
    $this->users = $users;
	}

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
	public function index()
  {
    $formations = $this->formations->all();
		$positions = $this->positions->all();
    $seasons = $this->seasons->all();
    $users = $this->users->all();
		$is_season_active = FALSE;
		foreach($seasons as $season)
    {
      if($season->is_active)
      {
        $is_season_active = TRUE;
      }
    }
    $positions_column_class = 'col-sm-' . max(1, (12 / count($positions)));
		return view('admin.seasons.index.main', compact('formations', 'is_season_active', 'positions_column_class', 'positions', 'seasons', 'users'));
	}

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function show(Season $season)
  {
    // Get Markets
		$markets = $this->markets->getSeasonMarkets($season->id);

    // Get fantateams
		$fantateams = $season->fantateams;

    // Get Formations
    $formations = $season->formations;

    // Get all formations
    $all_formations = $this->formations->all();

    // Remaining Formations Available
		foreach($all_formations as $idx => $formation)
    {
      if($formations->contains($formation->id))
      {
        unset($all_formations[$idx]);
      }
    }

    // Get All Users
		$all_users = $this->users->all();

		// Season's Users
		$season_users = $season->users;

    // Users not assigned to this season
		$rem_users = array();
		foreach($all_users as $user)
    {
      if(!$season_users->contains($user->id))
      {
        $rem_users[] = $user;
      }
    }

    // Users not assigned to a Fantateam
		$rem_season_users = array();
		foreach($season_users as $user)
    {
			$found = FALSE;
			foreach($fantateams as $fantateam)
      {
				if($fantateam->user_id == $user->id)
        {
					$found = TRUE;
					break;
				}
			}
			if($found === FALSE)
      {
        $rem_season_users[] = $user;
      }
		}
    $days = $season->days;
		return view('admin.seasons.show.main', compact('all_formations', 'all_users', 'days', 'fantateams', 'formations', 'markets', 'rem_season_users', 'rem_users', 'season', 'season_users'));
	}

  /**
   * Display the markets belonging to the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function showMarkets($id)
  {
    // Get
    $season = $this->seasons->find($id);
    if($season === null)
    {
      abort(404, 'Season not found.');
    }

    // Get markets
    $markets = $season->markets;

    // Get fantateams
    $fantateams = $season->fantateams;

		return view('admin.markets.main', compact('fantateams', 'markets', 'season'));
	}

  /**
   * Display the players belonging to the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function showPlayers($id)
  {
    // Get
    $season = $this->seasons->find($id);
    if($season === null)
    {
      abort(404, 'Season not found.');
    }

    // Get players
    $players = $this->players->getSeasonPlayers($id);

    // Get unconfirmed players
    $unconfirmed_players = $this->unconfirmed_players->getSeasonUnconfirmedPlayers($id);

		return view('admin.players.main', compact('season','players','unconfirmed_players'));
	}

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
	public function store(Request $request)
  {
    $this->validate($request, [
			'name' => 'required|max:255|unique:seasons,name',
			'number_days' => 'required|digits_between:1,40',
			'fantateam_budget' => 'required|min:1',
			'date_started' => 'required|date',
			'date_finished' => 'required|date',
			'formations' => 'required|array',
      'formations.*' => 'required|integer|exists:formations,id',
			'positions' => 'required|array',
      'positions.*.id' => 'required|integer|exists:positions,id',
			'positions.*.count' => 'required|integer|min:1',
			'users' => 'required|array',
      'users.*' => 'required|integer|exists:users,id',
		]);

    // Create
    $season = $this->seasons->create($request->all());

		return back();
	}

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function destroy(Season $season)
  {
    // Delete
    $this->seasons->destroy($season->id);

		return back();
	}

  /**
   * Attach a user to the specified resource in storage.
   *
   * @param int $id The user id.
   * @param int \Illuminate\Http\Request
   * @return  \Illuminate\Http\Response
   */
	public function addUser($id, Request $request)
  {
    // Get season
    $season = $this->seasons->find($id);

    // Validate
  	$this->validate($request, [
			'user_id' => 'required|integer|exists:users,id|unique:user_season,user_id,NULL,id,season_id,' . $id,
		]);

		if(count($season->users) == 0)
    {
      // Promote User to Administrator if no user available - Bad?
			$this->users->update($request->user_id, [
        'is_admin' => 1,
      ]);
		}

    // Attach user to Season
    if($season->users->contains($request->user_id) === FALSE)
    {
      $season->users()->attach($request->user_id);
    }

		return back();
	}

  /**
   * Detaches a user from the specified resource in storage.
   *
   * @param int $id The season id.
   * @param int $user_id The user id.
   * @return  \Illuminate\Http\Response
   */
	public function removeUser($id, $user_id)
  {
    $season->users()->detach($user_id);

    // Delete all fantateams associated to the user (!!!).
    // TODO move to repository
		$season->fantateams()->where('user_id', $user_id)->delete();

    return back();
	}

  /**
   * Attaches a formation to the specified resource in storage.
   *
   * @param int $id The season id.
   * @param int \Illuminate\Http\Request
   * @return  \Illuminate\Http\Response
   */
	public function addFormation($id, Request $request)
  {
  	$this->validate($request, [
			'formation_id' => 'required|exists:formations,id',
		]);

		if($season->formations->contains($request->formation_id) === FALSE)
    {
      // Attach formation to season  only if N/A
      $season->formations()->attach($request->formation_id);
  	}
		return back();
	}

  /**
   * Detaches a formation from the specified resource in storage.
   *
   * @param int $id The season id.
   * @param int $formation_id The formation id.
   * @return  \Illuminate\Http\Response
   */
	public function removeFormation($id, $formation_id)
  {
    // Detach formation
    $season->formations()->detach($formation_id);

		return back();
	}

  /**
   * Advance the specified resource to the next day.
   *
   * @param int $id The id of the season.
   * @return  \Illuminate\Http\Response
   */
	public function nextDay($id)
  {
    // Get Season
    $season = $this->seasons->find($id);

    // TODO improve process, possibly using repositories
  	$max = $season->days->max('day_number');
		if($season->current_day > $max)
    {
			$this->seasons->update($id, ['current_day' => $max,]);
		}
    else if($season->current_day < $max)
    {
      // TODO improve process, possibly using repositories
      $season->increment('current_day');
    }

		return back();
	}

  /**
   * Restart the specified resource.
   *
   * @param int $id The id of the season.
   * @return  \Illuminate\Http\Response
   */
	public function restart($id)
  {
    // Set current day to 0 (restart)
    $this->seasons->update($id, [
      'current_day' => 0,
    ]);

    return back();
	}

  /**
   * Start the specified resource.
   *
   * @param int $id The id of the season.
   * @return  \Illuminate\Http\Response
   */
	public function start($id)
  {
    // Get season
    $season = $this->seasons->find($id);
    if($season === null)
    {
      abort(404, 'Season not found.');
    }

    // "Stop" all other seasons
    $this->seasons->stopAll();

    // Set active flag to 1 and current day to 0 (start)
		$this->seasons->update($season->id, [
      'is_active' => 1,
      'current_day' => 0,
    ]);

		return back();
	}

  /**
   * Stop the specified resource.
   *
   * @param int $id The season id.
   * @return  \Illuminate\Http\Response
   */
	public function stop($id)
  {
    // Get season
    $season = $this->seasons->find($id);
    if($season === null)
    {
      abort(404, 'Season not found.');
    }

    // Set active flag to 0 (stop)
    $this->seasons->update($id, [
      'is_active' => 0,
    ]);

		return back();
	}

  /**
   * Confirm all unconfirmed players from the specified resource in storage.
   *
   * @param int $id The id of the season.
   * @return  \Illuminate\Http\Response
   */
	public function confirmAllUnconfirmedPlayers($id)
  {
    // Get season
    $season = $this->seasons->find($id);
    if($season === null)
    {
      abort(404, 'Season not found.');
    }

    // Confirmed all players
    // TODO flash variable if some could not be confirmed?
    $failed_confirm_player_ids = $this->unconfirmed_players->confirmAll($season->id);

		return back();
	}
}
