<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Fantateam;
use App\Repositories\FantateamRepository;
use App\Repositories\SeasonRepository;

class FantateamController extends Controller
{
	/**
	 * The fantateam repository implementation.
	 *
	 * @var FantateamRepository
	 */
	protected $fantateams;

	/**
	 * The fantateam repository implementation.
	 *
	 * @var SeasonRepository
	 */
	protected $seasons;

	/**
	 * Create a new controller instance.
	 *
	 * @param	FantateamRepository	$fantateams
	 * @param	SeasonRepository	$seasons
	 * @return void
	 */
	public function __construct(FantateamRepository $fantateams, SeasonRepository $seasons)
	{
		$this->fantateams = $fantateams;
		$this->seasons = $seasons;
	}

	/**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
	public function store(Request $request)
  {
		// Validate
		$this->validate($request, [
			'budget' => 'required|integer|min:1',
			'formation_id' => 'present|exists:formations,id',
			'season_id' => 'required|integer|exists:seasons,id',
			'user_id' => 'required|exists:users,id',
		]);

		// Get season
		$season = $this->seasons->find($request->season_id);

		$this->validate($request, [
			'budget' => 'in:'.$season->fantateam_budget,
			'name' => 'required|max:255|unique:fantateams,name,NULL,id,season_id,' . $request->season_id,
		]);

		if($season->is_active && $season->current_day == 0)
    {
      // Create
			$this->fantateams->create($request->all());
		}
		return back();
	}

	/**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function update(Request $request, $id)
	{
		// Get fantateam
		$fantateam = $this->fantateams->find($id);

		// No need to check if found as done by middleware already

		// Authorize
		$this->authorize('edit', $fantateam);

		// Get season
		$season = $this->seasons->find($fantateam->season_id);
		if($season === null)
		{
			abort(404, 'Season not found.');
		}

		// Validate
		$this->validate($request, [
			'budget' => 'in:'.$season->fantateam_budget,
			'formation_id' => 'present|exists:formations,id',
			'name' => 'required|max:255',
			'user_id' => 'required|exists:users,id',
		]);

		// Update
		$this->fantateams->update($id, $request->all());

		// No need to check if null as done by middleware already

		return back();
	}

	/**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
	public function destroy($id)
	{
		// Get fantateam
		$fantateam = $this->fantateams->find($id);

		// No need to check if found as done by middleware in advance

		// Authorize
		$this->authorize('destroy', $fantateam);

		// Can delete only if season not started yet
		if($fantateam->season->current_day == 0)
		{
			// Delete
			$response = $this->fantateams->destroy($fantateam->id);
		}

		return back();
	}
}
