<?php

/*
|--------------------------------------------------------------------------
| Unconfirmed Players routes File
|--------------------------------------------------------------------------
*/

// Only Admins can perform actions
Route::group(['middleware' => ['web', 'auth', 'admin']], function () {

	Route::group(['prefix' => 'unconfirmed_players'], function () {

		Route::post('upload', 'UnconfirmedPlayerController@upload');
		Route::post('fetch', 'UnconfirmedPlayerController@fetch');
		Route::delete('{id}', 'UnconfirmedPlayerController@destroy');
		Route::patch('{player}/confirm', 'UnconfirmedPlayerController@confirm');
	});
});
