<?php

/*
|--------------------------------------------------------------------------
| Seasons routes File
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['web', 'auth', 'admin']], function () {
	// Admin area get routes
	Route::group(['prefix' => 'admin/seasons'], function () {
		// Seasons
		Route::get('/', 'SeasonController@index');
		Route::get('{season}', 'SeasonController@show');
		Route::get('{season}/markets', 'SeasonController@showMarkets');
		Route::get('{season}/players', 'SeasonController@showPlayers');
	});

	// Other Resourceful routes
	Route::group(['prefix' => 'seasons'], function () {
		Route::post('/', 'SeasonController@store');
		Route::delete('{season}', 'SeasonController@destroy');
		Route::patch('{season}/start', 'SeasonController@start');
		Route::patch('{season}/confirm_all_unconfirmed_players', 'SeasonController@confirmAllUnconfirmedPlayers');

		Route::group(['middleware' => 'is_season_active'], function() {

			Route::patch('{id}/restart', 'SeasonController@restart');
			Route::patch('{id}/stop', 'SeasonController@stop');
			Route::patch('{id}/next_day', 'SeasonController@nextDay');

			// Formations
			Route::post('{id}/formations', 'SeasonController@addFormation');
			Route::delete('{id}/formations/{formation_id}', 'SeasonController@removeFormation');

			// Users
			Route::post('{id}/users', 'SeasonController@addUser');
			Route::delete('{id}/users/{user_id}', 'SeasonController@removeUser');
		});
	});
});
