<?php

/*
|--------------------------------------------------------------------------
| Formations routes File
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['web', 'auth', 'admin']], function () {
	Route::group(['prefix' => 'admin/formations'], function () {
		Route::get('/', 'FormationController@index');
	});
	Route::group(['prefix' => 'formations'], function () {
		Route::post('/', 'FormationController@store');
		Route::delete('{id}', 'FormationController@destroy');
	});
});
