<?php

/*
|--------------------------------------------------------------------------
| Fantateams routes File
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'fantateams', 'middleware' => ['web', 'auth']], function () {
	Route::group(['middleware' => 'is_fantateam_season_active'], function () {
		Route::put('{id}', 'FantateamController@update');

		Route::group(['middleware' => 'admin'], function () {
			Route::delete('{id}', 'FantateamController@destroy');
		});
	});

	Route::group(['middleware' => 'is_season_active'], function() {
		Route::post('/', 'FantateamController@store');
	});
});
