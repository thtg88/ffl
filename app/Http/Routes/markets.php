<?php

/*
|--------------------------------------------------------------------------
| Markets routes File
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['web', 'auth']], function () {
	// Admin functions
	Route::group(['middleware' => 'admin'], function () {

		Route::get('admin/markets/{id}', 'MarketController@show');

		Route::group(['prefix' => 'markets'], function () {

			Route::post('/', 'MarketController@store');
			Route::put('{id}', 'MarketController@update');
			Route::patch('{id}/close', 'MarketController@close');

			Route::group(['middleware' => 'is_market_finished'], function() {

				Route::delete('{id}', 'MarketController@destroy');

				Route::post('{id}/fantateams', 'MarketController@addFantateam');
				Route::delete('{id}/fantateams/{fantateam_id}', 'MarketController@removeFantateam');
			});
		});
	});

	Route::get('markets/{market}/players/{player}', 'MarketController@showPlayerOffers');
});
