<?php

/*
|--------------------------------------------------------------------------
| Players routes File
|--------------------------------------------------------------------------
*/

//  routes
Route::group(['prefix' => 'players', 'middleware' => ['web', 'auth', 'admin']], function () {
	Route::delete('{id}', 'PlayerController@destroy');
});
