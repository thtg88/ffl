<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'web'], function () {
	// Auth routes
	Route::auth();

	Route::group(['middleware' => 'auth'], function() {
		// Home routes
		Route::get('/', 'HomeController@index');

		Route::group(['middleware' => 'admin'], function() {
			// Admin Routes
			Route::get('admin', 'AdminController@index');
		});
	});
});
