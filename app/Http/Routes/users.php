<?php

/*
|--------------------------------------------------------------------------
| Users routes File
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['web', 'auth', 'admin']], function () {
	Route::group(['prefix' => 'admin/users'], function () {
		Route::get('/', 'UserController@index');
	});
	Route::group(['prefix' => 'users'], function () {
		Route::post('/', 'UserController@store');
		Route::delete('{id}', 'UserController@destroy');
	});
});
