<?php

/*
|--------------------------------------------------------------------------
| Offers routes File
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'offers', 'middleware' => ['web', 'auth']], function () {

	Route::group(['middleware' => 'is_market_finished'], function() {

		Route::post('/', 'OfferController@store');
	});

	Route::delete('{offer}', 'OfferController@destroy');
	Route::put('{offer}', 'OfferController@update');
});
