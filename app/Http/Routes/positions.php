<?php

/*
|--------------------------------------------------------------------------
| Positions routes File
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['web', 'auth', 'admin']], function () {
	Route::group(['prefix' => 'admin/positions'], function () {
		Route::get('/', 'PositionController@index');
	});
	Route::group(['prefix' => 'positions'], function () {
		Route::post('/', 'PositionController@store');
		Route::delete('{id}', 'PositionController@destroy');
	});
});
