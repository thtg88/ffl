<?php

/*
|--------------------------------------------------------------------------
| Teams routes File
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['web', 'auth', 'admin']], function () {
	Route::group(['prefix' => 'admin/teams'], function () {
		Route::get('/', 'TeamController@index');
		Route::get('{id}', 'TeamController@show');
	});
	Route::group(['prefix' => 'teams'], function () {
		Route::post('/', 'TeamController@store');
		Route::delete('{id}', 'TeamController@destroy');
	});
});
