<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
	protected $fillable = [
		'logo_path',
		'name',
	];

  public function players()
	{
		return $this->hasMany('App\Player');
	}
}
