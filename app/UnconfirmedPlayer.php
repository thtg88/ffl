<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnconfirmedPlayer extends Model
{
	protected $fillable = [
		'assists',
		'cost',
		'games_played',
		'goals_scored_conceived',
		'magic_avg_percent',
		'magic_points_percent',
		'mark_avg_percent',
		'name',
		'penalties_missed',
		'penalties_saved',
		'penalties_scored_saved',
		'penalties_shot',
		'position_id',
		'red_cards',
		'season_id',
		'team_id',
		'yellow_cards',
	];

  public function team()
	{
		return $this->belongsTo('App\Team');
	}

	public function position()
	{
		return $this->belongsTo('App\Position');
	}

	public function season()
	{
		return $this->belongsTo('App\Season');
	}
}
