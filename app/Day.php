<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
	protected $fillable = [
		'date_finished',
		'date_started',
		'day_number',
		'season_id',
	];
	protected $dates = [
		'date_finished',
		'date_started',
	];

	public function season()
	{
		return $this->belongsTo('App\Season');
	}
}
