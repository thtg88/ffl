<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Player extends Model
{
	use SoftDeletes;

	protected $dates = [
		'created_at',
		'deleted_at',
		'updated_at',
	];

  protected $fillable = [
		'assists',
		'cost',
		'fantateam_id',
		'games_played',
		'goals_scored_conceived',
		'mark_avg_percent',
		'magic_avg_percent',
		'magic_points_percent',
		'name',
		'penalties_shot',
		'penalties_scored_saved',
		'penalties_missed',
		'penalties_saved',
		'position_id',
		'red_cards',
		'season_id',
		'team_id',
		'yellow_cards',
	];

	public function setMarkAvgPercentAttribute($value)
	{
		$this->attributes['mark_avg_percent'] *= 100;
	}

	public function setMagicAvgPercentAttribute($value)
	{
		$this->attributes['magic_avg_percent'] *= 100;
	}

	public function setMagicPointsPercentAttribute($value)
	{
		$this->attributes['magic_points_percent'] *= 100;
	}

	public function getMarkAvgPercentAttribute()
	{
		return $this->attributes['mark_avg_percent'] / 100;
	}

	public function getMagicAvgPercentAttribute()
	{
		return $this->attributes['magic_avg_percent'] / 100;
	}

	public function getMagicPointsPercentAttribute()
	{
		return $this->attributes['magic_points_percent'] / 100;
	}

  public function team()
	{
		return $this->belongsTo('App\Team');
	}

	public function fantateam()
	{
		return $this->belongsTo('App\Fantateam');
	}

	public function position()
	{
		return $this->belongsTo('App\Position');
	}

	public function season()
	{
		return $this->belongsTo('App\Season');
	}

	public function offers()
	{
		return $this->hasMany('App\Offer');
	}
}
