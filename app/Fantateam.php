<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fantateam extends Model
{
	protected $fillable = [
		'budget',
		'formation_id',
		'name',
		'season_id',
		'user_id',
	];

	public function setFormationIdAttribute($value)
	{
		$this->attributes['formation_id'] = empty($value) ? NULL : $value;
	}

	public function formation()
	{
		return $this->belongsTo('App\Formation');
	}

	public function season()
	{
		return $this->belongsTo('App\Season');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function players()
	{
		return $this->hasMany('App\Player');
	}

	public function offers()
	{
		return $this->hasMany('App\Offer');
	}

	public function markets()
	{
		return $this->belongsToMany('App\Market', 'market_fantateam', 'fantateam_id', 'market_id');
	}
}
