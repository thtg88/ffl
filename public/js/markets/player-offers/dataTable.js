function initPlayerOffersDataTable() {
	return $('#player_offers-table').DataTable({
		"order": [[0, 'desc']],
		"aoColumns": [ null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}
