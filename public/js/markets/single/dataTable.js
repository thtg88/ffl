function initOffersDataTable() {
	return $('#offers-table').DataTable({
		"aoColumns": [ null,null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}

function initFantateamsDataTable() {
	return $('#fantateams-table').DataTable({
		"aoColumns": [ null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}
