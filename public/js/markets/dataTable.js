function initMarketsDataTable() {
	return $('#markets-table').DataTable({
		"aoColumns": [ null,null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}
