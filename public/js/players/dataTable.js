function initPlayersDataTable() {
	return $('#players-table').DataTable({
		"aoColumns": [ null,null,null,null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}

function initUnconfirmedPlayersDataTable() {
	return $('#unfonfirmed_players-table').DataTable({
		"aoColumns": [ null,null,null,null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}
