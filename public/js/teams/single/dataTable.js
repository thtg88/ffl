function initTeamPlayersDataTable() {
	return $('#team_players-table').DataTable({
		"aoColumns": [ null,null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}
