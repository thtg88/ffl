function initTeamsDataTable() {
	return $('#teams-table').DataTable({
		"aoColumns": [ null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}
