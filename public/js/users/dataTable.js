function initUsersDataTable() {
	return $('#users-table').DataTable({
		"aoColumns": [ null,null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}
