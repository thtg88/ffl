function initFormationsDataTable() {
	return $('#formations-table').DataTable({
		"aoColumns": [ null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}
