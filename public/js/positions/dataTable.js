function initPositionsDataTable() {
	return $('#positions-table').DataTable({
		"aoColumns": [ null,null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}
