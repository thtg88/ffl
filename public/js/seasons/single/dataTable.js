function initDaysDataTable() {
	return $('#days-table').DataTable({
		"aoColumns": [ null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}

function initFantateamsDataTable() {
	return $('#fantateams-table').DataTable({
		"aoColumns": [ null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}

function initFormationsDataTable() {
	return $('#formations-table').DataTable({
		"aoColumns": [ null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}

function initUsersDataTable() {
	return $('#users-table').DataTable({
		"aoColumns": [ null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}
