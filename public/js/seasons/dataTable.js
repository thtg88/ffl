function initSeasonsDataTable() {
	return $('#seasons-table').DataTable({
		"aoColumns": [ null,null,null,null,null,{ "bSortable": false } ],
		"bRetrieve": true
	});
}
