var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
  // App Styles
  mix.styles([
    'bootstrap.min.css',
    'dataTables.bootstrap.min.css',
  ], 'public/css/all.css');
  // App JavaScripts
  mix.scripts([
    'jquery.min.js',
    'bootstrap.min.js',
    'jquery.dataTables.min.js',
    'dataTables.bootstrap.min.js',
  ], 'public/js/app.js');
  // Version files for cache bursting
  mix.version([
    'css/all.css',
    'js/app.js',
  ]);
});
