<?php

use Illuminate\Database\Seeder;

class FormationTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('formations')->insert([
      'name' => "3-4-3",
    ]);
    DB::table('formations')->insert([
      'name' => "3-5-2",
    ]);
    DB::table('formations')->insert([
      'name' => "4-3-3",
    ]);
    DB::table('formations')->insert([
      'name' => "4-4-2"
    ]);
    DB::table('formations')->insert([
      'name' => "4-5-1",
    ]);
    DB::table('formations')->insert([
      'name' => "5-3-2",
    ]);
    DB::table('formations')->insert([
      'name' => "5-4-1",
    ]);

    $formations = DB::table('formations')->get();
    $positions = DB::table('positions')->get();

    foreach($formations as $formation)
    {
      $formationArr = explode("-", $formation->name);
      foreach($positions as $position)
      {
        $iteration = 0;
        switch($position->abbreviation)
        {
          case "P":
            DB::table('formation_position')->insert([
              'position_id' => $position->id,
              'formation_id' => $formation->id,
            ]);
            break;
          case "D":
            $iteration = $formationArr[0];
            break;
          case "C":
            if(isset($formationArr[1]))
            {
              $iteration = $formationArr[1];
            }
            break;
          case "A":
            $last = count($formationArr) - 1;
            if(isset($formationArr[$last]))
            {
              $iteration = $formationArr[$last];
            }
            break;
        }
        for($i = 0; $i < $iteration; $i++)
        {
          DB::table('formation_position')->insert([
            'position_id' => $position->id,
            'formation_id' => $formation->id,
          ]);
        }
      }
    }
  }
}
