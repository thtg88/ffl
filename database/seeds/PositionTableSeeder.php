<?php

use Illuminate\Database\Seeder;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('positions')->insert([
			'name' => "Goalkeeper",
			'abbreviation' => "P",
			'sequence' => 1,
		]);
		DB::table('positions')->insert([
			'name' => "Defender",
			'abbreviation' => "D",
			'sequence' => 2,
		]);
		DB::table('positions')->insert([
			'name' => "Midfielder",
			'abbreviation' => "C",
			'sequence' => 3,
		]);
		DB::table('positions')->insert([
			'name' => "Forward",
			'abbreviation' => "A",
			'sequence' => 5,
		]);
    }
}
