<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $this->call('PositionTableSeeder');
    $this->call('FormationTableSeeder');
    // Normalize milli-second datetime problem in SQL Server / Carbon
    DB::update('UPDATE formations SET [created_at]=DATEADD(ms,-DATEPART(ms,[created_at]),[created_at]),[updated_at]=DATEADD(ms,-DATEPART(ms,[updated_at]),[updated_at])');
    DB::update('UPDATE positions SET [created_at]=DATEADD(ms,-DATEPART(ms,[created_at]),[created_at]),[updated_at]=DATEADD(ms,-DATEPART(ms,[updated_at]),[updated_at])');
    DB::update('UPDATE formation_position SET [created_at]=DATEADD(ms,-DATEPART(ms,[created_at]),[created_at]),[updated_at]=DATEADD(ms,-DATEPART(ms,[updated_at]),[updated_at])');
  }
}
