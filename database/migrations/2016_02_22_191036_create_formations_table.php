<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormationsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('formations', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name', 10);
      $table->timestamp('created_at')->default(\DB::raw("GETUTCDATE()"));
      $table->timestamp('updated_at')->default(\DB::raw("GETUTCDATE()"));
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('formations');
  }
}
