<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketFantateamTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('market_fantateam', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('market_id');
      $table->integer('fantateam_id');
      $table->timestamp('created_at')->default(\DB::raw("GETUTCDATE()"));
      $table->timestamp('updated_at')->default(\DB::raw("GETUTCDATE()"));
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('market_fantateam');
  }
}
