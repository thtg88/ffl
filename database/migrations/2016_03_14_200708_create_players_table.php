<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('players', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->integer('season_id');
      $table->integer('position_id');
      $table->integer('fantateam_id')->nullable();
      $table->integer('team_id')->nullable();
      $table->integer('cost');
      $table->integer('games_played');
      $table->integer('goals_scored_conceived');
      $table->integer('assists');
      $table->integer('yellow_cards');
      $table->integer('red_cards');
      $table->integer('penalties_shot');
      $table->integer('penalties_scored_saved');
      $table->integer('penalties_missed');
      $table->integer('penalties_saved');
      $table->integer('mark_avg_percent');
      $table->integer('magic_avg_percent');
      $table->integer('magic_points_percent');
      $table->timestamp('created_at')->default(\DB::raw("GETUTCDATE()"));
      $table->timestamp('updated_at')->default(\DB::raw("GETUTCDATE()"));
      $table->softDeletes();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('players');
  }
}
