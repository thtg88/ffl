<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonPositionTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('season_position', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('season_id');
			$table->integer('position_id');
			$table->timestamp('created_at')->default(\DB::raw("GETUTCDATE()"));
			$table->timestamp('updated_at')->default(\DB::raw("GETUTCDATE()"));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('season_position');
	}
}
