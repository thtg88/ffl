<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('seasons', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->tinyInteger('number_days');
      $table->tinyInteger('current_day')->default(\DB::raw("((0))"));
      $table->integer('fantateam_budget');
      $table->timestamp('date_started')->nullable();
      $table->timestamp('date_finished')->nullable();
      $table->boolean('is_active')->default(\DB::raw("((0))"));
      $table->timestamp('created_at')->default(\DB::raw("GETUTCDATE()"));
      $table->timestamp('updated_at')->default(\DB::raw("GETUTCDATE()"));
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('seasons');
  }
}
